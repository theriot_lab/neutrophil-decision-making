%% Script for automating stimulation experiments and data collection

% Continuous (and late) competition assay
% Early (5-pulse) competition assay
% Continuous reversal assay
% Early (5-pulse) reversal assay
% Late reversal assay

%% Continuous Competition Experiment - Opsin version - stimulate left competing front
% continuous stimulation
wells=m.Plate.wellName(~strcmp(m.wellLabels,''));
wells=sort(wells);
numFramesBefore = 5;
numFramesAfter1 = 250;
imgChannel='yfp';% this is for img the birfucations
frapStimLabel='frap407';  % frapGFP %50 ms %redFrap %tk445
imagingChannel={'tomkat'}; %'tirf'
bounds=[100 0.4e4];
timeInterval=3;
targetAngle0=pi/2; %downward;
targetPt=frapPos;  
frapSpotLocation=frapPos - [12 0]; % The minus [12 0] is to account for the empirical offset between the imaged frap spot and the effective spot
thresh=1100; 
objectiveMag=60;
minSize=600;
clear imOut
frameDiff=3; %number of frame difference between current and previous img for finding cell edges;
global keyPressFlag
sharpParam=[4 5]; 
m.updateObjectiveMagAndPixelSize;

numCells=0;
targetNumCells=100;
wellNum=0;
wellMin=0;
wellMax=40;


while numCells<targetNumCells
    % Search for a cell
    successBool = false;
    while ~successBool
        wellNum=mod((wellNum-wellMin+1),(wellMax-wellMin+1))+wellMin;
        m.gotoWell(wells(wellNum));  % Not using the offsets for manual cell selection
        
        datadir2=[dataDir '\ScanImages'];
        imYFP=m.snapImage(imgChannel,'save',false,'show',false,'displayWindow','right');
        imRFP=m.snapImage('tomkat','save',true,'datadir',datadir2,'show',false,'bounds',bounds,'displayWindow','left');
        imRFP=squeeze(imRFP(:,:,1));
        figure(1); set(gcf,'Position',[10 200 1800 750]); subplot(1,2,1);
        showImagesMergeChannels(imRFP,shiftMatrix(imYFP,dxCorrection,dyCorrection));
        response=input('Select cell? (y/n) ','s');
        if boolRegExp(response,'^[Yy]')
            subplot(1,2,2);
            m.moveStageToUserSelectedPoint('yfp',bounds);
            successBool=true;
        end
        close(1);
        fprintf('%s:%i   z=%.1f ,',wells{wellNum},successBool,m.Z)
    end
    
    if successBool  %start imaging block
        keyPressFlag = false;
        numCells=numCells+1;
        counter=counter+1;
        datadir1=[dataDir '\' m.well '_' m.wellLabel '_Cell' num2str(counter)]
        mkdir([m.RootDataDir filesep datadir1]);
        fid=fopen([m.RootDataDir filesep datadir1 filesep 'stage_positions.txt'],'w');
        xy0=m.XY;
        fprintf(fid,'Z=%.1f   X=%.1f   Y=%.1f\n',m.Z,m.XY);
        
        % Image before stimulation
        numBefore = 0;
        conditionFlag=false;
        edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
        while ~conditionFlag
            t0=nowInSec;
            numBefore = numBefore +1;
            fprintf(fid,'frame=%i\n',numBefore);
            fprintf(fid,'Initial coordinates: %.1f, %.1f\n',m.XY);
            img=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
            set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
            if numBefore == 1
                try
                 [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(img);
                 m.wait;
                catch
                    continue  % OK want to be sure we get a good initial cell
                end
            end
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(img,initialCellCentroid,channelAngle, 0 ,sharpParam);
            if numBefore > frameDiff & (edgeL(numBefore)-edgeC(numBefore) > 10) & (edgeR(numBefore)-edgeC(numBefore) > 10)   % 10 pixel threshold for branches exceeding the center, change that to 30 for LATE COMPETITION experiment
                % compute target location
                if 1==1 % always stimulated the left competing front
                    maskChosen=maskL{numBefore}>0;  % The greater than zero is included to make sure that the mask is of type "logical" - that is how the gotoCellEdgePoint function detects that it is a mask rather than an image
                    try 
                    
                    pt = getBifurcationFrapTargetPt(maskL{numBefore},maskL{numBefore-frameDiff});
                    catch
                        continue  % OK - want to be sure we get a good initial stimulation site
                    end
                    branchChoice='LEFT';
                    fprintf('Selecting the LEFT branch.\n');
                else
                    maskChosen=maskR{numBefore}>0;
                    pt = getBifurcationFrapTargetPt(maskR{numBefore},maskR{numBefore-frameDiff});
                    branchChoice='RIGHT';
                    fprintf('Selecting the RIGHT branch.\n');
                end
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                fprintf(fid,'Frap spot coordinates (pixels): %.0f, %.0f\n',frapSpotLocation);
                fprintf(fid,'Stimulation branch choice = %s\n',branchChoice);
                conditionFlag=true;
                m.XY=xy0;
                m.wait;
                fprintf(fid,'Post-stim coordinates: %.1f, %.1f\n',m.XY);
            end
            subplot(1,2,1); hold on; 
            if keyPressFlag
                break;
            end
            pause(timeInterval - (nowInSec-t0));
        end
        edgeCIn=edgeC(numBefore);
        
        % Image after stimulation
        if ~keyPressFlag
            for i=1:numFramesAfter1
                t0=nowInSec;
                fprintf(fid,'frame=%i\n',i);
                imOut=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
                set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
                
                try
                    [edgeL(numBefore+i), edgeC(numBefore+i), edgeR(numBefore+i), maskL{numBefore+i},...
                        maskC{numBefore+i}, maskR{numBefore+i}] =...
                        getCellEdgePositionsInBifurcationAngleMethod(imOut,initialCellCentroid,channelAngle,edgeCIn, sharpParam);
                catch
                    edgeL(numBefore+i)=edgeL(numBefore+i-1);
                    edgeC(numBefore+i)=edgeC(numBefore+i-1);
                    edgeR(numBefore+i)=edgeR(numBefore+i-1);
                    maskL(numBefore+i)=maskL(numBefore+i-1);
                    maskC(numBefore+i)=maskC(numBefore+i-1);
                    maskR(numBefore+i)=maskR(numBefore+i-1);
                    fprintf('Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                    fprintf(fid,'Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                end  
                
                if strcmp(branchChoice,'LEFT')        % choose the left branch of the cell to target
                   try
                    pt1 = getBifurcationFrapTargetPt(maskL{numBefore+i},maskL{numBefore+i-frameDiff});
                   catch
                       fprintf('Failed target site determination. Continuing.\n');
                       fprintf(fid,'Failed target site determination. Continuing.\n');
                   end
                else
                    pt1 = getBifurcationFrapTargetPt(maskR{numBefore+i},maskR{numBefore+i-frameDiff});
                end
                if sum(pt1)>0
                    pt=pt1;
                end
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.XY=xy0;
                if keyPressFlag
                    break;
                end
                pause(timeInterval - (nowInSec-t0));
            end
        end
        fclose(fid);
    end %imaging block
end
fprintf('Done.\n');

%% Early Competition Experiment - Opsin version - stimulate left branch
% 5 light pulses only, early stimulation assay
wells=m.Plate.wellName(~strcmp(m.wellLabels,''));
wells=sort(wells);
numFramesBefore = 5;
numFramesAfter1 = 150;
maxNumStim = 4; % This will be the number of additional stimulation pulses delivered, after the first (total of 5 pulses)
stimulateYN=false;
frapStimLabel='frap407';  % frapGFP %50 ms %redFrap %tk445
imagingChannel={'tomkat'}; %'tirf'
imgChannel='yfp';% this is for img the birfucations
bounds=[100 0.4e4];
timeInterval=3;
sharpParam=[4 5]; 
targetAngle0=pi/2; %downward;
targetPt= frapPos;  
frapSpotLocation=frapPos - [12 0]; % The minus [12 0] is to account for the empirical offset between the imaged frap spot and the effective spot
thresh=1100; 
objectiveMag=60;
minSize=600;
clear imOut
frameDiff=3; %number of frame difference between current and previous img for finding cell edges;
global keyPressFlag
sharpParm=[4 5];
m.updateObjectiveMagAndPixelSize;

numCells=0;
targetNumCells=100;
wellNum=0;
wellMin=0;
wellMax=80;


while numCells<targetNumCells
    % Search for a good cell
    successBool = false;
    while ~successBool
        wellNum=mod((wellNum-wellMin+1),(wellMax-wellMin+1))+wellMin;
        m.gotoWell(wells(wellNum));  % Not using the offsets for manual cell selection 
        datadir2=[dataDir '\ScanImages'];
        imYFP=m.snapImage(imgChannel,'save',false,'show',false,'displayWindow','right');
        %imYFP=refImg{wellNum};
        imRFP=m.snapImage('tomkat','save',true,'datadir',datadir2,'show',false,'bounds',bounds,'displayWindow','left');
        imRFP=squeeze(imRFP(:,:,1));
        figure(1); set(gcf,'Position',[10 200 1800 750]); subplot(1,2,1);
        showImagesMergeChannels(imRFP,shiftMatrix(imYFP,dxCorrection,dyCorrection));
        response=input('Select cell? (y/n) ','s');
        if boolRegExp(response,'^[Yy]')
            subplot(1,2,2);
            m.moveStageToUserSelectedPoint('yfp',bounds);
            successBool=true;
        end
        close(1);
        fprintf('%s:%i ,',wells{wellNum},successBool)
    end
    
    if successBool  %start imaging block
        keyPressFlag = false;
        numCells=numCells+1;
        counter=counter+1;
        datadir1=[dataDir '\' m.well '_' m.wellLabel '_Cell' num2str(counter)]
        mkdir([m.RootDataDir filesep datadir1]);
        fid=fopen([m.RootDataDir filesep datadir1 filesep 'stage_positions.txt'],'w');
        xy0=m.XY;
        
        % Image before stimulation
        numBefore = 0;
        conditionFlag=false;
        edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
        while ~conditionFlag
            t0=nowInSec;
            numBefore = numBefore +1;
            fprintf(fid,'frame=%i\n',numBefore);
            fprintf(fid,'Initial coordinates: %.1f, %.1f\n',m.XY);
            img=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
            set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
            if numBefore == 1
                try
                 [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(img);
                 m.wait;
                catch
                    continue
                end
            end
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(img,initialCellCentroid,channelAngle, 0, sharpParam);
            if numBefore > frameDiff & (edgeL(numBefore)-edgeC(numBefore) > 10) & (edgeR(numBefore)-edgeC(numBefore) > 10)   % 10 pixel distance threshold for branches exceeding the center
                % compute target location
                if 1==1                 % choose the left branch of the cell to target
                    maskChosen=maskL{numBefore}>0;  % The greater than zero is included to make sure that the mask is of type "logical" - that is how the gotoCellEdgePoint function detects that it is a mask rather than an image
                    try 
                    
                    pt = getBifurcationFrapTargetPt(maskL{numBefore},maskL{numBefore-frameDiff});
                    catch
                        continue
                    end
                    branchChoice='LEFT';
                    fprintf('Selecting the LEFT branch.\n');
                else
                    maskChosen=maskR{numBefore}>0;
                    pt = getBifurcationFrapTargetPt(maskR{numBefore},maskR{numBefore-frameDiff});
                    branchChoice='RIGHT';
                    fprintf('Selecting the RIGHT branch.\n');
                end
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                fprintf(fid,'Frap spot coordinates (pixels): %.0f, %.0f\n',frapSpotLocation);
                fprintf(fid,'Stimulation branch choice = %s\n',branchChoice);
                conditionFlag=true;
                m.XY=xy0;
                m.wait;
                fprintf(fid,'Post-stim coordinates: %.1f, %.1f\n',m.XY);
            end
            subplot(1,2,1); hold on; 
            if keyPressFlag
                break;
            end
            pause(timeInterval - (nowInSec-t0));
        end
        edgeCIn=edgeC(numBefore);
        
        % Image after stimulation
        if ~keyPressFlag
            stimCount=0;
            for i=1:numFramesAfter1
                t0=nowInSec;
                fprintf(fid,'frame=%i\n',i);
                imOut=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
                set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
                
                if stimCount<maxNumStim   % Only do the stimulation if we haven't reached the max number already
                    try
                        [edgeL(numBefore+i), edgeC(numBefore+i), edgeR(numBefore+i), maskL{numBefore+i},...
                            maskC{numBefore+i}, maskR{numBefore+i}] = ...
                            getCellEdgePositionsInBifurcationAngleMethod(imOut,initialCellCentroid,channelAngle,edgeCIn,sharpParam);
                    catch
                        edgeL(numBefore+i)=edgeL(numBefore+i-1);
                        edgeC(numBefore+i)=edgeC(numBefore+i-1);
                        edgeR(numBefore+i)=edgeR(numBefore+i-1);
                        maskL(numBefore+i)=maskL(numBefore+i-1);
                        maskC(numBefore+i)=maskC(numBefore+i-1);
                        maskR(numBefore+i)=maskR(numBefore+i-1);
                        fprintf('Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                        fprintf(fid,'Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                    end
                    
                    if strcmp(branchChoice,'LEFT')        % choose the left branch of the cell to target
                        try
                            pt1 = getBifurcationFrapTargetPt(maskL{numBefore+i},maskL{numBefore+i-frameDiff});
                        catch
                        end
                    else
                        pt1 = getBifurcationFrapTargetPt(maskR{numBefore+i},maskR{numBefore+i-frameDiff});
                    end
                    if sum(pt1)>0
                        pt=pt1;
                    end
                    m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                    m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                    stimCount=stimCount+1;
                    fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                    subplot(1,2,1); hold on;
                    scatter(pt(1),pt(2),'r','LineWidth',3);
                    m.XY=xy0;
                end
                if keyPressFlag
                    break;
                end
                pause(timeInterval - (nowInSec-t0));
            end
        end
        fclose(fid);
    end %imaging block
end
fprintf('Done.\n');

%% Continuous Reversal Experiment - Opsin version - stimulate the losing branch
%continuous reversal stimulation
counter=0;
wells=m.Plate.wellName(~strcmp(m.wellLabels,''));
wells=sort(wells);
numFramesBefore = 5;
numFramesAfter1 = 350;
frapStimLabel='frap407';  % frapGFP %50 ms %redFrap %tk445
imagingChannel={'tomkat'};
imgChannel='yfp';% this is for img the birfucations
bounds=[100 0.4e4];
timeInterval=3;
targetAngle0=pi/2; %downward; 
targetPt=frapPos;
frapSpotLocation=frapPos - [12 0]; % The minus [12 0] is to account for the empirical offset between the imaged frap spot and the effective spot
thresh=1100; 
objectiveMag=60;
minSize=600;
clear imOut
frameDiff=2; %number of frame difference between current and previous img for finding cell edges;
retractionThresh=-150; %150 threshold to determine if a big enough retraction is occurring.
global keyPressFlag
sharpParam=[4 5];
%Save a copy of the script to the data folder to have a record of what was used.
Version='1';
fName='2019-08-05-Script-Cdc42TK-Opsin_competition and reversal';
scriptDir='C:\Users\Collins Lab\Documents\MATLAB\AmaliaH';
scriptNameAndLocation=[scriptDir filesep fName];
FileNameAndLocation=[m.RootDataDir filesep dataDir filesep fName];
newbackup=sprintf('%s-backup%s.m',FileNameAndLocation,Version);
currentfile=strcat(scriptNameAndLocation, '.m');
copyfile(currentfile,newbackup);

% save MetaData file

if exist([m.RootDataDir filesep dataDir filesep 'metaData'], 'file') ==0
    writeScopeMetaDataToFile(m,[m.RootDataDir filesep dataDir],...
        imagingChannel,timeInterval,frapSpotLocation,targetAngle0);
else
    fprintf('Warning that file already exists, change the file name to avoid overwriting');
end

m.updateObjectiveMagAndPixelSize;

numCells=0;
targetNumCells=100;
wellNum=0;
wellMin=1;
wellMax=60;

while numCells<targetNumCells
    % Search for a good cell
    successBool = false;
    while ~successBool
        wellNum=mod((wellNum-wellMin+1),(wellMax-wellMin+1))+wellMin;
        m.gotoWell(wells(wellNum));  % Not using the offsets for manual cell selection
        
        datadir2=[dataDir '\ScanImages'];
        imYFP=m.snapImage(imgChannel,'save',false,'show',false,'displayWindow','right');
        %imYFP=refImg{wellNum};
        imRFP=m.snapImage('tomkat','save',true,'datadir',datadir2,'show',false,'bounds',bounds,'displayWindow','left');
        imRFP=squeeze(imRFP(:,:,1));
        figure(1); set(gcf,'Position',[10 200 1800 750]); subplot(1,2,1);
        showImagesMergeChannels(imRFP,shiftMatrix(imYFP,dxCorrection,dyCorrection));
        response=input('Select cell? (y/n) ','s');
        if boolRegExp(response,'^[Yy]')
            subplot(1,2,2);
            m.moveStageToUserSelectedPoint('yfp',bounds);
            successBool=true;
        end
        close(1);
        fprintf('%s:%i z=%.1f,',wells{wellNum},successBool,m.Z)
    end
    
    if successBool  %start imaging block
        keyPressFlag = false;
        numCells=numCells+1;
        counter=counter+1;
        datadir1=[dataDir '\' m.well '_' m.wellLabel '_Cell' num2str(counter)]
        mkdir([m.RootDataDir filesep datadir1]);
        fid=fopen([m.RootDataDir filesep datadir1 filesep 'stage_positions.txt'],'w');
        xy0=m.XY;
        fprintf(fid,'Z=%.1f   X=%.1f   Y=%.1f\n',m.Z,m.XY);

        % Image before stimulation
        numBefore = 0;
        conditionFlag=false;
        edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
        while ~conditionFlag
            t0=nowInSec;
            numBefore = numBefore +1;
            fprintf(fid,'frame=%i\n',numBefore);
            fprintf(fid,'Initial coordinates: %.1f, %.1f\n',m.XY);
            img=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
            set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
            if numBefore == 1
                try
                [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(img);
                 m.wait;
                catch
                    continue  % OK - want to be sure we start with a good cell and image
                end
            end
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(img,initialCellCentroid,channelAngle,0,sharpParam);
            if numBefore-frameDiff>0
                [bool,branchChoice] = isRetractionStarted(maskL{numBefore},maskL{numBefore-frameDiff},maskR{numBefore},maskR{numBefore-frameDiff},retractionThresh);
            else
                bool=false;
            end
            if bool   % 10 pixel distance threshold for branches exceeding the center
                % compute target location
                if strcmp(branchChoice,'LEFT')       
                    maskChosen=maskL{numBefore}>0;  % The greater than zero is included to make sure that the mask is of type "logical" - that is how the gotoCellEdgePoint function detects that it is a mask rather than an image
                    branchChoice='LEFT';
                    fprintf('Selecting the LEFT branch.\n');
                else
                    maskChosen=maskR{numBefore}>0;
                    branchChoice='RIGHT';
                    fprintf('Selecting the RIGHT branch.\n');
                end
                try
                    pt=getBifurcationFrapTargetPtForRetractionExperiment(maskChosen);
                catch
                    fprintf('Error caught - initial stimulation site determination\n');
                    continue  % OK - want to be sure we get a good initial stimulation site
                end
                
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                fprintf(fid,'Frap spot coordinates (pixels): %.0f, %.0f\n',frapSpotLocation);
                fprintf(fid,'Stimulation branch choice = %s\n',branchChoice);
                conditionFlag=true;
                m.XY=xy0;
                m.wait;
                fprintf(fid,'Post-stim coordinates: %.1f, %.1f\n',m.XY);
            end
            subplot(1,2,1); hold on; 
            if keyPressFlag
                break;
            end
            pause(timeInterval - (nowInSec-t0));
        end
        edgeCIn=edgeC(numBefore);
        
        % Image after stimulation
        if ~keyPressFlag
            for i=1:numFramesAfter1
                
                  if keyPressFlag
                    break;
                end
                
                t0=nowInSec;
                fprintf(fid,'frame=%i\n',i);
                imOut=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
                set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
                
                try
                    [edgeL(numBefore+i), edgeC(numBefore+i), edgeR(numBefore+i),...
                        maskL{numBefore+i}, maskC{numBefore+i}, maskR{numBefore+i}] =...
                        getCellEdgePositionsInBifurcationAngleMethod(imOut,initialCellCentroid,channelAngle,edgeCIn,sharpParam);
                catch
                    edgeL(numBefore+i)=edgeL(numBefore+i-1);
                    edgeC(numBefore+i)=edgeC(numBefore+i-1);
                    edgeR(numBefore+i)=edgeR(numBefore+i-1);
                    maskL(numBefore+i)=maskL(numBefore+i-1);
                    maskC(numBefore+i)=maskC(numBefore+i-1);
                    maskR(numBefore+i)=maskR(numBefore+i-1);
                    fprintf('Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                    fprintf(fid,'Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                end
                
                if strcmp(branchChoice,'LEFT')        
                    maskChosen=maskL{numBefore+i}>0;
                else
                    maskChosen=maskR{numBefore+i}>0;
                end
                try
                    pt1 = getBifurcationFrapTargetPtForRetractionExperiment(maskChosen);
                catch
                    % pt1 should retain its previous value
                end
                if sum(pt1)>0
                    pt=pt1;
                end
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.XY=xy0;
                pause(timeInterval - (nowInSec-t0));
            end
        end
        fclose(fid);
    end %imaging block
end
fprintf('Done.\n');

%% Early Reversal Experiment - Opsin version - stimulate the losing branch right after retraction starts
% 5 light pulses immediately post retraction
wells=m.Plate.wellName(~strcmp(m.wellLabels,''));
wells=sort(wells);
numFramesBefore = 5;
numFramesAfter1 = 350;
maxNumStim = 5; 
stimulateYN=false;
frapStimLabel='frap407';  
imagingChannel={'tomkat'};
imgChannel='yfp'; % this is for img the bifurcation channels
bounds=[100 0.4e4];
timeInterval=3;
targetAngle0=pi/2;
targetPt=frapPos;   
frapSpotLocation=frapPos - [12 0]; % The minus [12 0] is to account for the empirical offset between the imaged frap spot and the effective spot
thresh=1100;
objectiveMag=60;
minSize=600;
clear imOut+
frameDiff=2; %number of frame difference between current and previous img for finding cell edges;
retractionThresh=-150; %threshold to determine if a big enough retraction is occurring.
global keyPressFlag
sharpParam=[4 5]; % use this for the Cdc42 cells. For Myosin cells leave blank or run at [4 50];

m.updateObjectiveMagAndPixelSize;

numCells=0;
targetNumCells=100;
wellNum=0;
wellMin=0;
wellMax=80;

while numCells<targetNumCells
    % Search for a good cell
    successBool = false;
    while ~successBool
        wellNum=mod((wellNum-wellMin+1),(wellMax-wellMin+1))+wellMin;
        m.gotoWell(wells(wellNum));  % Not using the offsets for manual cell selection
        datadir2=[dataDir '\ScanImages'];
        imYFP=m.snapImage(imgChannel,'save',false,'show',false,'displayWindow','right');
        imRFP=m.snapImage('tomkat','save',true,'datadir',datadir2,'show',false,'bounds',bounds,'displayWindow','left');
        imRFP=squeeze(imRFP(:,:,1));
        figure(1); set(gcf,'Position',[10 200 1800 750]); subplot(1,2,1);
        showImagesMergeChannels(imRFP,shiftMatrix(imYFP,dxCorrection,dyCorrection));
        response=input('Select cell? (y/n) ','s');
        if boolRegExp(response,'^[Yy]')
            subplot(1,2,2);
            m.moveStageToUserSelectedPoint('yfp',bounds);
            successBool=true;
        end
        close(1);
        fprintf('%s:%i ,',wells{wellNum},successBool)
    end
    
    if successBool  %start imaging block
        keyPressFlag = false;
        numCells=numCells+1;
        counter=counter+1;
        stimCount=0;
        datadir1=[dataDir '\' m.well '_' m.wellLabel '_Cell' num2str(counter)]
        mkdir([m.RootDataDir filesep datadir1]);
        fid=fopen([m.RootDataDir filesep datadir1 filesep 'stage_positions.txt'],'w');
        xy0=m.XY;
        
        % Image before stimulation
        numBefore = 0;
        conditionFlag=false;
        edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
        while ~conditionFlag
            t0=nowInSec;
            numBefore = numBefore +1;
            fprintf(fid,'frame=%i\n',numBefore);
            fprintf(fid,'Initial coordinates: %.1f, %.1f\n',m.XY);
            img=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
            set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
            if numBefore == 1
                try
                 [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(img);
                 m.wait;
                catch
                    continue  % OK program may crash if we hit this error
                end
            end
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(img,initialCellCentroid,channelAngle,0,sharpParam);
            if numBefore-frameDiff>0
                [bool,branchChoice] = isRetractionStarted(maskL{numBefore},maskL{numBefore-frameDiff},maskR{numBefore},maskR{numBefore-frameDiff},retractionThresh);
            else
                bool=false;
            end
            if bool   % 10 pixel distance threshold for branches exceeding the center
                % compute target location
                if strcmp(branchChoice,'LEFT')        
                    maskChosen=maskL{numBefore}>0;  % The greater than zero is included to make sure that the mask is of type "logical" - that is how the gotoCellEdgePoint function detects that it is a mask rather than an image
                    branchChoice='LEFT';
                    fprintf('Selecting the LEFT branch.\n');
                else
                    maskChosen=maskR{numBefore}>0;
                    branchChoice='RIGHT';
                    fprintf('Selecting the RIGHT branch.\n');
                end
                try
                    pt=getBifurcationFrapTargetPtForRetractionExperiment(maskChosen);
                catch
                    fprintf('Error caught - initial stimulation site determination\n');
                    continue  %OK - make sure we get a good initial stimulation
                end
                
                subplot(1,2,1); hold on;
                scatter(pt(1),pt(2),'r','LineWidth',3);
                m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                stimCount=stimCount+1;
                fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                fprintf(fid,'Frap spot coordinates (pixels): %.0f, %.0f\n',frapSpotLocation);
                fprintf(fid,'Stimulation branch choice = %s\n',branchChoice);
                conditionFlag=true;
                m.XY=xy0;
                m.wait;
                fprintf(fid,'Post-stim coordinates: %.1f, %.1f\n',m.XY);
            end
            subplot(1,2,1); hold on; 
            if keyPressFlag
                break;
            end
            pause(timeInterval - (nowInSec-t0));
        end
        edgeCIn=edgeC(numBefore);
        
        % Image after stimulation
        if ~keyPressFlag
            for i=1:numFramesAfter1
                t0=nowInSec;
                fprintf(fid,'frame=%i\n',i);
                imOut=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
                set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
                
                if stimCount<maxNumStim   % Only stimulate if we have not yet reached the max number of stimulations
                    try
                        [edgeL(numBefore+i), edgeC(numBefore+i), edgeR(numBefore+i),...
                            maskL{numBefore+i}, maskC{numBefore+i}, maskR{numBefore+i}] =...
                            getCellEdgePositionsInBifurcationAngleMethod(imOut,initialCellCentroid,channelAngle,edgeCIn,sharpParam);
                    catch
                        edgeL(numBefore+i)=edgeL(numBefore+i-1);
                        edgeC(numBefore+i)=edgeC(numBefore+i-1);
                        edgeR(numBefore+i)=edgeR(numBefore+i-1);
                        maskL(numBefore+i)=maskL(numBefore+i-1);
                        maskC(numBefore+i)=maskC(numBefore+i-1);
                        maskR(numBefore+i)=maskR(numBefore+i-1);
                        fprintf('Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                        fprintf(fid,'Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                    end
                    
                    if strcmp(branchChoice,'LEFT')        
                        maskChosen=maskL{numBefore+i}>0;
                    else
                        maskChosen=maskR{numBefore+i}>0;
                    end
                    try
                        pt1 = getBifurcationFrapTargetPtForRetractionExperiment(maskChosen);
                    catch
                    end
                    if sum(pt1)>0
                        pt=pt1;
                    end
                    m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                    m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                    stimCount=stimCount+1;
                    fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                    subplot(1,2,1); hold on;
                    scatter(pt(1),pt(2),'r','LineWidth',3);
                    m.XY=xy0;
                end
                if keyPressFlag
                    break;
                end
                pause(timeInterval - (nowInSec-t0));
            end
        end
        fclose(fid);
    end %imaging block
end
fprintf('Done.\n');

%% Late Reversal Experiment - Opsin version - stimulate the losing branch only once retraction is almost complete
wells=m.Plate.wellName(~strcmp(m.wellLabels,''));
wells=sort(wells);
numFramesBefore = 5;
numFramesAfter1 = 250;
stimulateYN=false;
frapStimLabel='frap407';  
imagingChannel={'tomkat'};
imgChannel='yfp'; % this is for img the bifurcations channels
bounds=[100 0.4e4];
timeInterval=3;
targetAngle0=pi/2; %downward; 
targetPt=frapPos;
frapSpotLocation=frapPos - [12 0]; % The minus [12 0] is to account for the empirical offset between the imaged frap spot and the effective spot
thresh=1100; 
objectiveMag=60;
minSize=600;
clear imOut
frameDiff=2; %number of frame difference between current and previous img for finding cell edges;
retractionThresh=-150; %threshold to determine if a big enough retraction is occurring.
retractionCompleteThresh=10; %threshold to determine when the retraction has almost completed
global keyPressFlag
sharpParam=[4 5];
%Save a copy of the script to the data folder to have a record of what was used.
Version='1';
fName='2019-05-28-Script-Cdc42TK-Opsin_competition and reversal';
scriptDir='C:\Users\Collins Lab\Documents\MATLAB\AmaliaH';
scriptNameAndLocation=[scriptDir filesep fName];
FileNameAndLocation=[m.RootDataDir filesep dataDir filesep fName];
newbackup=sprintf('%s-backup%s.m',FileNameAndLocation,Version); 
currentfile=strcat(scriptNameAndLocation, '.m');
copyfile(currentfile,newbackup);

% save MetaData file

if exist([m.RootDataDir filesep dataDir filesep 'metaData'], 'file') ==0
    writeScopeMetaDataToFile(m,[m.RootDataDir filesep dataDir],...
        imagingChannel,timeInterval,frapSpotLocation,targetAngle0);
else
    fprintf('Warning that file already exists, change the file name to avoid overwriting');
end

m.updateObjectiveMagAndPixelSize;

numCells=0;
targetNumCells=100;
wellNum=0; 
wellMin=0;
wellMax=40;

while numCells<targetNumCells
    % Search for a good cell
    successBool = false;
    while ~successBool
        wellNum=mod((wellNum-wellMin+1),(wellMax-wellMin+1))+wellMin;
        m.gotoWell(wells(wellNum));  % Not using the offsets for manual cell selection
        
        datadir2=[dataDir '\ScanImages'];
        imYFP=m.snapImage(imgChannel,'save',false,'show',false,'displayWindow','right');
        imRFP=m.snapImage('tomkat','save',true,'datadir',datadir2,'show',false,'bounds',bounds,'displayWindow','left');
        imRFP=squeeze(imRFP(:,:,1));
        figure(1); set(gcf,'Position',[10 200 1800 750]); subplot(1,2,1);
        showImagesMergeChannels(imRFP,shiftMatrix(imYFP,dxCorrection,dyCorrection));
        response=input('Select cell? (y/n) ','s');
        if boolRegExp(response,'^[Yy]')
            subplot(1,2,2);
            m.moveStageToUserSelectedPoint('yfp',bounds);
            successBool=true;
        end
        close(1);
        fprintf('%s:%i z=%.1f,',wells{wellNum},successBool,m.Z)
    end
    
    if successBool  %start imaging block
        keyPressFlag = false;
        numCells=numCells+1;
        counter=counter+1;
        datadir1=[dataDir '\' m.well '_' m.wellLabel '_Cell' num2str(counter)]
        mkdir([m.RootDataDir filesep datadir1]);
        fid=fopen([m.RootDataDir filesep datadir1 filesep 'stage_positions.txt'],'w');
        xy0=m.XY;
        fprintf(fid,'Z=%.1f   X=%.1f   Y=%.1f\n',m.Z,m.XY);

        % Image before stimulation
        numBefore = 0;
        conditionFlag=false;
        edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
        while ~conditionFlag
            t0=nowInSec;
            numBefore = numBefore +1;
            fprintf(fid,'frame=%i\n',numBefore);
            fprintf(fid,'Initial coordinates: %.1f, %.1f\n',m.XY);
            img=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
            set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
            if numBefore == 1
                try
                [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(img);
                 m.wait;
                catch
                    continue
                end
            end
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(img,initialCellCentroid,channelAngle,0,sharpParam);
            if numBefore-frameDiff>0
                [bool,branchChoice] = isRetractionStarted(maskL{numBefore},maskL{numBefore-frameDiff},maskR{numBefore},maskR{numBefore-frameDiff},retractionThresh);
            else
                bool=false;
            end
            if bool   % 10 pixel distance threshold for branches exceeding the center
                % compute target location
                if strcmp(branchChoice,'LEFT')       
                    maskChosen=maskL{numBefore}>0;  % The greater than zero is included to make sure that the mask is of type "logical" - that is how the gotoCellEdgePoint function detects that it is a mask rather than an image
                    branchChoice='LEFT';
                    fprintf('Selecting the LEFT branch.\n');
                else
                    maskChosen=maskR{numBefore}>0;
                    branchChoice='RIGHT';
                    fprintf('Selecting the RIGHT branch.\n');
                end
                
                fprintf(fid,'Frap spot coordinates (pixels): %.0f, %.0f\n',frapSpotLocation); 
                fprintf(fid,'Stimulation branch choice = %s\n',branchChoice);
                fprintf(fid,'Waiting for retraction to complete before stimulating\n');
                conditionFlag=true;
                m.wait;
            end
            if keyPressFlag
                break;
            end
            pause(timeInterval - (nowInSec-t0));
        end
        edgeCIn=edgeC(numBefore);
        
        % Image after retraction has started
        if ~keyPressFlag
            stimFlag=false;   %this flag will be flipped to TRUE once the retraction has nearly completed
            for i=1:numFramesAfter1
                t0=nowInSec;
                fprintf(fid,'frame=%i\n',i);
                imOut=m.snapImage(imagingChannel,'save',true,'datadir',datadir1,'show',true,'bounds',bounds,'displayWindow','left');
                set(gcf, 'KeyPressFcn', {@detectKeyPress}, 'UserData', []);
                
                try
                    [edgeL(numBefore+i), edgeC(numBefore+i), edgeR(numBefore+i),...
                            maskL{numBefore+i}, maskC{numBefore+i}, maskR{numBefore+i}] =...
                            getCellEdgePositionsInBifurcationAngleMethod(imOut,initialCellCentroid,channelAngle,edgeCIn,sharpParam);
                catch
                    edgeL(numBefore+i)=edgeL(numBefore+i-1);
                    edgeC(numBefore+i)=edgeC(numBefore+i-1);
                    edgeR(numBefore+i)=edgeR(numBefore+i-1);
                    maskL(numBefore+i)=maskL(numBefore+i-1);
                    maskC(numBefore+i)=maskC(numBefore+i-1);
                    maskR(numBefore+i)=maskR(numBefore+i-1);
                    fprintf('Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                    fprintf(fid,'Edge and mask detection failed at frame after = %i. Using previous values.\n',i);
                end
                
                if strcmp(branchChoice,'LEFT')      
                    maskChosen=maskL{numBefore+i}>0;
                    edgeRetracting=edgeL(numBefore+i);
                else
                    maskChosen=maskR{numBefore+i}>0;
                    edgeRetracting=edgeR(numBefore+i);
                end
                if edgeRetracting-edgeC(numBefore+i) < retractionCompleteThresh
                    stimFlag=true;
                end
                if stimFlag
                    try
                        pt1 = getBifurcationFrapTargetPtForRetractionExperiment(maskChosen);
                    catch
                    end
                    if sum(pt1)>0
                        pt=pt1;
                    end
                    m.moveStageToSpecifiedPoint('',[],pt,frapSpotLocation);
                    m.snapImage(frapStimLabel,'save',true,'datadir',datadir1,'show',false);
                    fprintf(fid,'Stimulation coordinates: %.1f, %.1f\n',m.XY);
                    subplot(1,2,1); hold on;
                    scatter(pt(1),pt(2),'r','LineWidth',3);
                    m.XY=xy0;
                end
                if keyPressFlag
                    break;
                end
                pause(timeInterval - (nowInSec-t0));
            end
        end
        fclose(fid);
    end %imaging block
end
fprintf('Done.\n');