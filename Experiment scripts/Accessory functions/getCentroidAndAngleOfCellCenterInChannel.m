function [centroid,angle] = getCentroidAndAngleOfCellCenterInChannel(img)
% This function will take a single image, and return the x coordinate
% (after cropping) that corresponds to the midline of the cell mask

%7/19/2019-GB Additions: I need to use this function in image processing as
%well. I added the if loop to determine if img is an actual image or if its
%a mask image. If its a mask, we assume that only one cell appears in the
%mask.


if ~islogical(img)
img=double(imageSubtractBackground(img(:,:,1),40,128));
imgC=img(100:900,412:612);  % Crop to keep only the center channel

% Get rid of cell pieces from the neighboring channels if they are present
% mask1=fretGetCellMasks_63x(imgC,{[1 1 200 800]});
% mask1=mask1-imclearborder(mask1);
% imgC(mask1>0)=0;

% Get cell mask
mask=fretGetCellMasks_63x(imgC,{[1 1 200 800]});
mask=imopen(mask,strel('disk',11));
targetRegion=false(size(mask));
targetRegion(100:512,(512-20-411):(512+20-411))=true; % image matrix coordinates are (y,x);
targetMask=bwareafilt(targetRegion & mask>0,1);
labelMask=bwlabel(mask);
cellInd=median(labelMask(targetMask));
mask=labelMask==cellInd;
else 
    mask=img(100:900,412:612);
end




res=regionprops(mask,'Orientation','Centroid');
angle=-1*res.Orientation*pi/180;  % Convert angle to radians, and orient the angle to be consistent with our coordinates
centroid=res.Centroid + [411 99];
