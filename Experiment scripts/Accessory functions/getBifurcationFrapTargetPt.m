function pt = getBifurcationFrapTargetPt(maskCurrent,maskPrevious)

if max(vect(imopen(maskCurrent-maskPrevious,strel('disk',1))))>0
    maskEdge=imopen(maskCurrent-maskPrevious>0,strel('disk',1));
    maskBotHalf=maskCurrent;
    d0=regionprops(maskCurrent,'Centroid');
    maskBotHalf(1:round(d0(1).Centroid(2)),:)=false;
    maskEdge=maskEdge & maskBotHalf;
    maskEdge=bwareafilt(maskEdge,1);
    d=regionprops(maskEdge,'Centroid');
    
    if length(d)==0
        pt=[0 0];
    else
        pt=d(1).Centroid;
    end
else
    pt=[0 0];
end