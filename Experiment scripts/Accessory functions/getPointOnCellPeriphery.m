function pt=getPointOnCellPeriphery(mask,targetAngle)

boundary=bwboundaries(mask);
boundary=boundary{1};
d=regionprops(mask,'Centroid');
for i=1:size(boundary,1)
    boundary(i,3)=vector2angle(boundary(i,2:-1:1) - d(1).Centroid);
end
if targetAngle<0.1 | targetAngle>(2*pi-0.1)
    targetAngle=mod(targetAngle+1,2*pi);
    boundary(:,3)=mod(boundary(:,3)+1,2*pi);
end

boundary(:,4)=abs(boundary(:,3)-targetAngle);

boundary=sortrows(boundary,4);

pt=boundary(1,1:2);
%keyboard;