function angle=vector2angle(vector,units)
%Takes a two element vector (x,y) and returns the angle of the direction of
%the vector (1,0) --> 0, (1,1) --> pi/4, etc. The result returned will be
%between 0 and 2*pi (angle >= 0 and angle < 2*pi)

if length(vector)~=2
    warning('Vector is of an unexpected size. Expecting length of 2.');
end
if nargin<2
    units='radians';
end
if boolRegExp({units},'^[Dd][Ee][Gg]')
    units='degrees';
end

if vector(1)<0
    angle = pi - atan(-1*vector(2)/vector(1));
else
    angle = atan(vector(2)/vector(1));
end
if angle<0
    angle = angle + 2*pi;
end

if strcmp(units,'degrees')
    angle=angle*180/pi;
end
