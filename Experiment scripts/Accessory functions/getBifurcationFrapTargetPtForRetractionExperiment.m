function pt = getBifurcationFrapTargetPtForRetractionExperiment(maskCurrent)

targetAngle=pi/2;

maskBotHalf=maskCurrent;
d0=regionprops(maskBotHalf,'Centroid');
maskBotHalf(1:round(d0(1).Centroid(2)),:)=false;
maskRbot=maskCurrent & maskBotHalf;
pt=getPointOnCellPeriphery(maskRbot,targetAngle);
pt=[pt(2) pt(1)];
