function mask = line2mask(pt1,angle,length,imsize,width)
% Assumes that angle is given in radians

mask=false(imsize);
xvals= round(pt1(1) + ((-1*length/2):(length/2))*cos(angle)); 
yvals= round(pt1(2) + ((-1*length/2):(length/2))*sin(angle));

for i=1:size(xvals,2)
    mask(yvals(i),xvals(i))=true;
end

mask=imdilate(mask,strel('disk',width));
