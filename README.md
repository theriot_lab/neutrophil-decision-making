This repository contains code in MATLAB and R written by Amalia Hadjitheodorou, George R. R. Bell and Sean R. Collins. The code was developed for automating experiments and data analysis, as a companion to the paper "Mechanical competition promotes selective listening to receptor inputs to resolve directional dilemmas in neutrophil migration", available online at: https://www.biorxiv.org/content/10.1101/2022.02.21.481331v1

It is not intended as a ready to use package for other data sets. 

The analysis code made use of Matlab's image processing and optimization tool boxes. The plotting scripts made use of Kelly Kearney's boundedline.m function.

How to cite this material:

doi: 10.5281/zenodo.7535222
