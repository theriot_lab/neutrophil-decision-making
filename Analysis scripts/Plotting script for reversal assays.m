%% Script to read in the analyzed .mat files and produce plots for Fig 5
% this script was used to compute and plot migration and signaling
% measuments pertaining to reversal datasets

%% Directories
masterRoot='/Volumes/LaCie/UC_Davis_Collab/Data/';
% Determine where your m-file's foclder is.
folder = '/Volumes/LaCie/UC_Davis_Collab/Code';
% Add that folder plus all subfolders to the path.
addpath(genpath(folder)); 

%% Read in curated files

load('/Volumes/LaCie/UC_Davis_Collab/Data/Cdc42RevDat/Cdc42Dat-ContinuousReversals.mat');

%% Preliminary analysis: optional 
% Separating out two datasets (successful and unsuccessful cont reversals)
% Note that here we are not considering the stallers

N = length(Cdc42RevDat); 
count=0;
count2=0;
for iFolder=1:N
    if Cdc42RevDat(iFolder).WinLose==1
        count=count+1;
        SuccessCdc42RevDat(count)=Cdc42RevDat(iFolder);
    else
        count2=count2+1;
        NoSuccessCdc42RevDat(count2)=Cdc42RevDat(iFolder);
    end 
end

%% Clear previous Cdc42Dat and keep only successful reversals

clear Cdc42RevDat
Cdc42RevDat=SuccessCdc42RevDat;
N = length(Cdc42RevDat); 

%% Load in data in auxialiary variables to later plot

tempScale= 3 / 60; % 3 sec time interval, dividing by 60sec to turn the speed into min
distanceScale = 0.4389 * 2 * (10/60) *1.5;

for iFolder=1:N
    try
    auxiliary=Cdc42RevDat(iFolder).maskFinal;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EntireCellMask(iFolder,1:length(Cdc42RevDat(iFolder).maskFinal))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).edgeMaskR400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EdgeMaskR400(iFolder,1:length(Cdc42RevDat(iFolder).edgeMaskR400))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).edgeMaskL400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EdgeMaskL400(iFolder,1:length(Cdc42RevDat(iFolder).edgeMaskL400))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_FrontL400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FrontLeftCdc42(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_FrontL400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_FrontR400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FrontRightCdc42(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_FrontR400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_Back400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    BackCdc42(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_Back400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_Cell;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EntireCellCdc42(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_Cell))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_SkeletonLength;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    SkeletonLength(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_SkeletonLength))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_Elongation;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    MajorAxis(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_Elongation))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42RevDat(iFolder).Ratio_AreaTotal;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    CellArea(iFolder,1:length(Cdc42RevDat(iFolder).Ratio_AreaTotal))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    FramesBeforeStim(iFolder)=Cdc42RevDat(iFolder).framesNoStimed;
    FramesStimed(iFolder)=Cdc42RevDat(iFolder).framesStimed;
    ExpType{iFolder}=Cdc42RevDat(iFolder).ExpType;  
    end
end

%% Find and assign which side retracted (was stimulated) and which one not

for iFolder=1:N
    folder=Cdc42RevDat(iFolder).folderPathCdc42;
    %Read stage positions to find retracting/stimed edge
    fid=fopen(['/Volumes/LaCie/UC_Davis_Collab/Data' filesep folder filesep 'stage_positions.txt'],'r');
    lines=readAllLines(fid);
    fclose(fid);
    if sum(contains(lines,'LEFT'))>0
        SideStim{iFolder}='LEFT';
        try
        StimulatedMaskEdge(iFolder,:)=EdgeMaskL400(iFolder,:);
        NonStimulatedMaskEdge(iFolder,:)=EdgeMaskR400(iFolder,:);
        Cdc42StimulatedEdge(iFolder,:)=FrontLeftCdc42(iFolder,:);
        Cdc42NonStimulatedEdge(iFolder,:)=FrontRightCdc42(iFolder,:);
        end
    else
        SideStim{iFolder}='RIGHT';
        try
        StimulatedMaskEdge(iFolder,:)=EdgeMaskR400(iFolder,:);
        NonStimulatedMaskEdge(iFolder,:)=EdgeMaskL400(iFolder,:);
        Cdc42StimulatedEdge(iFolder,:)=FrontRightCdc42(iFolder,:);
        Cdc42NonStimulatedEdge(iFolder,:)=FrontLeftCdc42(iFolder,:);
        end
    end
end

%% For successful reversals extract when a cell responds/repolarizes 
% This is based on a strategy similar to identifying the initiation of big
% retraction to start stimulating the cell: here we are identifying the
% frame we detect a big protrusion (>150 pixels)

frameDiff=2; % to avoid segmentation noise we are working over a 2 frame window
for iFolder=1:N
    clear EdgeAreaDiff yes value
    for i=FramesBeforeStim(iFolder)+frameDiff:find(~cellfun(@isempty,StimulatedMaskEdge(iFolder,:)),1,'last')
        if ~isnan(StimulatedMaskEdge{iFolder,i}) & ~isnan(StimulatedMaskEdge{iFolder,i-frameDiff})
            try
                [yes(i) value(i)]=isProtrusionStarted_v2(StimulatedMaskEdge{iFolder,i},StimulatedMaskEdge{iFolder,i-frameDiff},150);
                DiffProtrusionArea{iFolder,i}=value(i);
            catch
                yes(i)=nan;
                value(i)=nan;
            end
        else 
            yes(i)=nan;
            value(i)=nan;
        end
        StimFrame(iFolder)=(FramesBeforeStim(iFolder)+1);
        try
            ResponseFrame(iFolder)=find(yes,1,'first');
        catch
            ResponseFrame(iFolder)=nan;
        end
    end
end 

%% Plot averages for Cdc42 activity between big retraction and repolarization interpolated--block to generate Supplementary Fig. 2B,2F

% Define the relevant time for interpolation for each cell
for iFolder=1:N
    FrameDuration(iFolder)=(ResponseFrame(iFolder)-StimFrame(iFolder)); % to be used for interpolation
end
% Take the average number of frames across cells to be the number of timepoints we will interpolate
timepoints=round(nanmean(FrameDuration)); 

% Interpolate the edge Cdc42 on each edge 
clear interpolated_Cdc42Stimed interpolated_Cdc42NonStimed
for iFolder=1:N
    try
    oldvalsCdc42Stimed=Cdc42StimulatedEdge(iFolder,StimFrame(iFolder):ResponseFrame(iFolder));
    oldvalsCdc42NonStimed=Cdc42NonStimulatedEdge(iFolder,StimFrame(iFolder):ResponseFrame(iFolder));
    
    matrix1=[[1:FrameDuration(iFolder)+1];oldvalsCdc42Stimed]';
    [I1matrix1,I2matrix1]=fnSmoothCurve(matrix1,0.5,0,timepoints); %timepoints: interpolated pts 
    interpolated_Cdc42Stimed(iFolder,:)=I2matrix1(:,2);
    
    matrix2=[[1:FrameDuration(iFolder)+1];oldvalsCdc42NonStimed]';
    [I1matrix2,I2matrix2]=fnSmoothCurve(matrix2,0.5,0,timepoints); 
    interpolated_Cdc42NonStimed(iFolder,:)=I2matrix2(:,2);
    
    continue
    interpolated_Cdc42Stimed(iFolder,:)=nan;
    interpolated_Cdc42NonStimed(iFolder,:)=nan;
    end
end

% Clean up to remove any zero values
interpolated_Cdc42Stimed(interpolated_Cdc42Stimed==0)=nan;
interpolated_Cdc42NonStimed(interpolated_Cdc42NonStimed==0)=nan;
interpolated_Cdc42Overall(interpolated_Cdc42Overall==0)=nan;

interpolated_Cdc42Stimed=interpolated_Cdc42Stimed((all((~isnan(interpolated_Cdc42Stimed)),2)),:);
interpolated_Cdc42NonStimed=interpolated_Cdc42NonStimed((all((~isnan(interpolated_Cdc42NonStimed)),2)),:);
interpolated_Cdc42Overall=interpolated_Cdc42Overall((all((~isnan(interpolated_Cdc42Overall)),2)),:);

% Calculate average, std and sterr of the mean
N2=length(interpolated_Cdc42NonStimed); % Correct and compute the non nan rows
time=(0:(timepoints-1))./(timepoints-1); % This will be our interpolated time relevant to the big retraction

% Plotting the mean Cdc42 activity at each edge
mean_stimedge = nanmean(interpolated_Cdc42Stimed);
std_stimedge = nanstd(interpolated_Cdc42Stimed);
sterr_stimedge=std_stimedge./sqrt(N2);
mean_nonstimedge = nanmean(interpolated_Cdc42NonStimed);
std_nonstimedge = nanstd(interpolated_Cdc42NonStimed);
sterr_nonstimedge=std_nonstimedge./sqrt(N2);

figure()
boundedline(time,mean_stimedge,std_stimedge,'-b','transparency', 0.1)
hold on
plot(time,mean_stimedge,'-b','LineWidth',2)
hold on
boundedline(time,mean_nonstimedge ,std_nonstimedge,'-r','transparency', 0.1)
hold on
plot(time,mean_nonstimedge,'-r','LineWidth',2)
hold on
errorbar(time,mean_stimedge,sterr_stimedge,'b')
hold on
errorbar(time,mean_nonstimedge,sterr_nonstimedge,'r')
ylabel('Cdc42 at each edge')
xlabel('interpolated time between retraction and repolarization')
set(gca,'FontSize',20)
ylim([0.92 1.1])

%% Speed calculation leveraging protrusion/retraction areas--block to generate Supplementary Fig. 2A,2C,2E,2G
%compute retraction/protrusion areas

distanceScale = 0.4389 * 2 * (10/60) *1.5; 
tempScale= 3 / 60; %temporal scale for the speed calculations in min (frame interval=3s)
frameDiff=2; 
for iFolder=1:N % all movies
    for i=1+frameDiff:find(~cellfun(@isempty,StimulatedMaskEdge(iFolder,:)),1,'last')
        if ~isnan(StimulatedMaskEdge{iFolder,i}) & ~isnan(StimulatedMaskEdge{iFolder,i-frameDiff}) & ~isnan(NonStimulatedMaskEdge{iFolder,i}) & ~isnan(NonStimulatedMaskEdge{iFolder,i-frameDiff})
            try
                [yes StimedMaskDiff(i)]=isProtrusionStarted_v2(StimulatedMaskEdge{iFolder,i},StimulatedMaskEdge{iFolder,i-frameDiff},150);
                [yes NonStimedMaskDiff(i)]=isProtrusionStarted_v2(NonStimulatedMaskEdge{iFolder,i},NonStimulatedMaskEdge{iFolder,i-frameDiff},150);
                StimedAreaDiff{iFolder,i}=StimedMaskDiff(i);
                NonStimedAreaDiff{iFolder,i}=NonStimedMaskDiff(i);
            catch
                StimedMaskDiff(i)=nan;
                NonStimedMaskDiff(i)=nan;
            end
        end
    end
end

%fix the cell array
for iFolder=1:N
    auxiliary=StimedAreaDiff(iFolder,:);
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FixedStimedAreaDiff(iFolder,1:length(StimedAreaDiff(iFolder,:)))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    auxiliary=NonStimedAreaDiff(iFolder,:);
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FixedNonStimedAreaDiff(iFolder,1:length(NonStimedAreaDiff(iFolder,:)))=cell2mat(auxiliary); % this will be a cell array structure
end

% find the frame with zero retraction
for iFolder=1:N
    SmoothedFixedStimedAreaDiff(iFolder,:)=smooth(FixedStimedAreaDiff(iFolder,:),0.1,'loess');
    try
        timevector=StimFrame(iFolder):ResponseFrame(iFolder);
        P2 = InterX([timevector;SmoothedFixedStimedAreaDiff(iFolder,timevector(1):timevector(end))],[timevector;zeros(length(SmoothedFixedStimedAreaDiff(iFolder,timevector(1):timevector(end))),1)']);
        RetractionCompletedFrame(iFolder)=round(P2(1));
        clear P2
    catch
        RetractionCompletedFrame(iFolder)=nan;
    end
end

% Define the relevant timings
for iFolder=1:N
    StimToRetractionCompletion(iFolder)=(RetractionCompletedFrame(iFolder)-StimFrame(iFolder));
    RetractionCompletionToReprotrusion(iFolder)=(ResponseFrame(iFolder)-RetractionCompletedFrame(iFolder)); 
end

for iFolder=1:N % all movies
    clear n_matrix1 n_matrix2 n_I1matrix1 n_I2matrix1 n_I1matrix2 n_I2matrix2 oldvalsStimedMaskDiff oldvalsNonStimedMaskDiff
    try
    oldvalsStimedMaskDiff=FixedStimedAreaDiff(iFolder,StimFrame(iFolder):ResponseFrame(iFolder)); % relevant time of interest to interpolate
    oldvalsNonStimedMaskDiff=FixedNonStimedAreaDiff(iFolder,StimFrame(iFolder):ResponseFrame(iFolder));
    
    n_matrix1=[[1:FrameDuration(iFolder)+1];oldvalsStimedMaskDiff]';
    [n_I1matrix1,n_I2matrix1]=fnSmoothCurve(n_matrix1,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
    interpolated_StimedMaskDiff(iFolder,:)=n_I2matrix1(:,2);
    
    n_matrix2=[[1:FrameDuration(iFolder)+1];oldvalsNonStimedMaskDiff]';
    [n_I1matrix2,n_I2matrix2]=fnSmoothCurve(n_matrix2,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
    interpolated_NonStimedMaskDiff(iFolder,:)=n_I2matrix2(:,2);
    continue
    interpolated_StimedMaskDiff(iFolder,:)=nan;
    interpolated_NonStimedMaskDiff(iFolder,:)=nan;
    end
end 

% Clean up to remove any zero values
interpolated_StimedMaskDiff(interpolated_StimedMaskDiff==0)=nan;
interpolated_NonStimedMaskDiff(interpolated_NonStimedMaskDiff==0)=nan;

interpolated_StimedMaskDiff=interpolated_StimedMaskDiff((all((~isnan(interpolated_StimedMaskDiff)),2)),:);
interpolated_NonStimedMaskDiff=interpolated_NonStimedMaskDiff((all((~isnan(interpolated_NonStimedMaskDiff)),2)),:);

% Unit conversion
interpolated_StimedMaskDiff=interpolated_StimedMaskDiff.*(distanceScale*distanceScale/(frameDiff*tempScale));
interpolated_NonStimedMaskDiff=interpolated_NonStimedMaskDiff.*(distanceScale*distanceScale/(frameDiff*tempScale));

% Calculate average, std and sterr of the mean
time=0:timepoints-1; % This will be our interpolated time relevant to the big retraction
N2=length(interpolated_NonStimedMaskDiff); % Correct and compute the non nan rows
time=(0:(timepoints-1))./(timepoints-1); % This will be our interpolated time relevant to the big retraction

% Compute and plot speeds in the way of retraction/protrusion areas
mean_stimedgespeed = nanmean(interpolated_StimedMaskDiff);
std_stimedgespeed = nanstd(interpolated_StimedMaskDiff);
sterr_stimedgespeed=std_stimedgespeed./sqrt(N2);
mean_nonstimedgespeed = nanmean(interpolated_NonStimedMaskDiff);
std_nonstimedgespeed = nanstd(interpolated_NonStimedMaskDiff);
sterr_nonstimedgespeed=std_nonstimedgespeed./sqrt(N2);

figure()
boundedline(time,mean_stimedgespeed,std_stimedgespeed,'-b','transparency', 0.1)
hold on
plot(time,mean_stimedgespeed,'-b','LineWidth',2)
hold on
boundedline(time,mean_nonstimedgespeed ,std_nonstimedgespeed,'-r','transparency', 0.1)
hold on
plot(time,mean_nonstimedgespeed,'-r','LineWidth',2)
hold on
errorbar(time,mean_stimedgespeed,sterr_stimedgespeed,'b')
hold on
errorbar(time,mean_nonstimedgespeed,sterr_nonstimedgespeed,'r')
ylabel('area speed (microns^2/min)')
xlabel('interpolated time between retraction and repolarization')
set(gca,'FontSize',20)
%ylim([-150 220])
ylim([-120 150])

%% Positions in the channel coordinates--block to generate Fig. 5D

for iFolder=1:N
    SumProjectedMask{iFolder}=0;
    for i=1:find(~cellfun(@isempty,EntireCellMask(iFolder,:)),1,'last')
        SumProjectedMask{iFolder}=EntireCellMask{iFolder,i}+SumProjectedMask{iFolder};
    end
end

for iFolder=1:N
    clear p
    try
    % get the parameters through the function
    [p]=channelMapParameters(SumProjectedMask{iFolder});
    %compute the functionalized form of the channel
    channelMap=channelBranchSkeletonFunction(p);
    bwmask = im2bw(SumProjectedMask{iFolder},1);
    skeleton= bwskel(bwmask);
    t=-1.5:0.001:1.5; vals=channelMap.branches(p,t); 
    newskeletonmask = false(size(skeleton));    
    for indext=1:length(t)
          newskeletonmask(round(vals(2,indext)),round(vals(1,indext)))=true;  
    end
    
    OpositeChannelEndMask = false(size(newskeletonmask));
    OpositeChannelEndMask(round(vals(2,end)),round(vals(1,end)))=true;
    newapexmask = false(size(newskeletonmask));
    newapexmask(p(2), p(1))=true;
    
    D = bwdistgeodesic(newskeletonmask, OpositeChannelEndMask,'quasi-euclidean');
    distanceApexEnd=D(p(2), p(1));
    
    for i=1:find(~cellfun(@isempty,EntireCellMask(iFolder,:)),1,'last')
        try
        [edgePtL,edgePtR,edgeMaskL400,edgeMaskR400]=getBifurcationParametrizedEdgePts(EntireCellMask{iFolder,i},channelMap,p,400);     

          distanceL(iFolder,i)=distanceApexEnd-D(edgePtL(2), edgePtL(1)); % NEGATIVES in left
          distanceR(iFolder,i)=distanceApexEnd-D(edgePtR(2), edgePtR(1));
        catch
            distanceL(iFolder,i)=nan;
            distanceR(iFolder,i)=nan;
        end
    end
    end
end

for iFolder=1:N
    try
    if strcmp(SideStim{iFolder},'LEFT')
        distance(iFolder,:)=abs(distanceL(iFolder,:)).*distanceScale;
    else
        distance(iFolder,:)=distanceR(iFolder,:).*distanceScale;
    end
    try
    distanceStimStarted(iFolder)=distance(iFolder,StimFrame(iFolder));
    distanceReprotrusionStarted(iFolder)=distance(iFolder,ResponseFrame(iFolder));
    catch
        distanceStimStarted(iFolder)=nan;
        distanceReprotrusionStarted(iFolder)=nan;
    end 
    end
end

% find the min distance from apex the the stimulated edge reached post
% stimulation
for iFolder=1:N
    tend(iFolder)=FramesBeforeStim(iFolder)+FramesStimed(iFolder)-1;
    try
        Smoothed(iFolder,StimFrame(iFolder):tend(iFolder))=smooth(distance(iFolder,StimFrame(iFolder):tend(iFolder)),0.1,'loess');
        edgeatmaxdistance(iFolder)=min(Smoothed(iFolder,StimFrame(iFolder):tend(iFolder)));
    catch
        edgeatmaxdistance(iFolder)=nan;
    end
end

ratio=edgeatmaxdistance./distanceStimStarted;
ratio=(ratio(~isnan(ratio)));

indicesLongResponders=find((ratio<0.5)==1);
indicesFastResponders=find((ratio<0.5)==0);

figure()
hold on
scatter(distanceStimStarted(indicesLongResponders),edgeatmaxdistance(indicesLongResponders),70, 'b','filled')
xlabel('distance from apex when stim started')
ylabel('min distance from apex during retraction')
x = linspace(0,30,500) ;
y=x;
hold on
ylim([0 30])

%% Calculations of pre-existing asymmetries--block to generate Fig. 5E, 5F

format shortG
for iFolder=1:N
    %Cdc42 edge metric
    AverageCdc42StimulatedEdge(iFolder)=nanmean(Cdc42StimulatedEdge(iFolder,StimFrame(iFolder):StimFrame(iFolder)+1));
    AverageCdc42NonStimulatedEdge(iFolder)=nanmean(Cdc42NonStimulatedEdge(iFolder,StimFrame(iFolder):StimFrame(iFolder)+1));
    
    %Speed metric from areas
    AverageStimedAreaDiff(iFolder)=nanmean(FixedStimedAreaDiff(iFolder,StimFrame(iFolder):StimFrame(iFolder)+1))*(distanceScale*distanceScale/(frameDiff*tempScale));
    AverageNonStimedAreaDiff(iFolder)=nanmean(FixedNonStimedAreaDiff(iFolder,StimFrame(iFolder):StimFrame(iFolder)+1))*(distanceScale*distanceScale/(frameDiff*tempScale));
end

clear t1 t2
t1= AverageCdc42NonStimulatedEdge(indicesLongResponders);
t2= AverageCdc42NonStimulatedEdge(indicesFastResponders);
ranksum(t1,t2)

%% Explore distance travelled of retracting/repolarized edge

% figure()
% iFolder=1;
% frames=length(StimFrame(iFolder):ResponseFrame(iFolder)); 
% plot([1:size((distance(iFolder,:)),2)],distance(iFolder,:))
% 
% xlabel('frame number')
% ylabel('distance from apex for retracting side (left)')
% xlim([0 70])
% hold on
% scatter(StimFrame(iFolder),distance(iFolder,StimFrame(iFolder)))
% hold on
% scatter(ResponseFrame(iFolder),distance(iFolder,ResponseFrame(iFolder)))