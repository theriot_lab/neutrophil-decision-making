%% Script for analyzing the raw data to extract initial parameters
%% Initial pre-processing script
% The goal of this script is to process cells expressing the Cdc42 FRET
% biosensor and opsin that undergo stimulation. 
% Parameters extracted from the script:
% i) alignment parameters
% ii) frames before, stim initiation and frames after stim
% iii) experimental type and whether it was a win/loss
% iv) cell body masks
% iv) compute Cdc42 FRET ratio
% v) save computed parameters and cropped images

%% %% make save and script dirs

masterRoot='/Volumes/LaCie/UC_Davis_Collab/Data/5thVisit/'; 
% Determine where your m-file's folder is.
folder = '/Volumes/LaCie/UC_Davis_Collab/Code'; 
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

%% Use excel sheet to import good cell folders
[~,sheets]=xlsfinfo([masterRoot 'Scripts' filesep '5thVisitCumulativeCtr.xlsx']);
clear infoStruct;
for i=1:length(sheets)
    [~,~, raw]= xlsread([masterRoot 'Scripts' filesep '5thVisitCumulativeCtr.xlsx'],i);
    infoStruct(i).condition=sheets{i};
    cellNums=cell2mat(raw(2:end,1));
    tempFolders=raw(2:end,2);
    infoStruct(i).folders=tempFolders(cellNums>0);
    tempWinLose=cell2mat(raw(2:end,3));
    tempTypeExp=(raw(2:end,4));
    WinLose(i).folders=tempWinLose(cellNums>0);
    typeExp(i).folders=tempTypeExp(cellNums>0);
    if boolRegExp(sheets(i),'Cdc42')
        infoStruct(i).channel1='TomKat-1';
        infoStruct(i).channel2='TomKat-2';
    elseif boolRegExp(sheets(i),'Myosin')
        infoStruct(i).channel1='PIP3Dual-1';
        infoStruct(i).channel2='PIP3Dual-2';
    end  
    infoStruct(i).channel3='FRAP'; %to be used for extracting the timestaps of the Frap stims
end

%% camera and donut corrections

% % 2nd and 3rd visit had the same camera corrections
load('/Volumes/LaCie/UC_Davis_Collab/Data/3rdVisit/Scripts/Image Corrections/Camera corrections 2018-08-09.mat',...
    'donutCorrection1','donutCorrection2','halfCorrection1','halfCorrection2','medianDark1', 'medianDark2');
load('/Volumes/LaCie/UC_Davis_Collab/Data/3rdVisit/Scripts/Cdc42-RatioCorrection-Channels/2D ratio correction-60xTK.mat','ratioCorrection');
TKratioCorr=double(ratioCorrection); clear ratioCorrection;

 %% camera and donut corrections for visit 4 and 5

 load('/Volumes/LaCie/UC_Davis_Collab/Data/4rthVisit/Scripts/Image Corrections/Camera correctionsTK 2020-03-23.mat',...
    'donutCorrection1','donutCorrection2','halfCorrection1','halfCorrection2','medianDark1', 'medianDark2');
load('/Volumes/LaCie/UC_Davis_Collab/Data/3rdVisit/Scripts/Cdc42-RatioCorrection-Channels/2D ratio correction-60xTK.mat','ratioCorrection');
TKratioCorr=double(ratioCorrection); clear ratioCorrection;

%% save Data in a matfile for future use
formatOut='yyyy-mm-dd';
fileNameStr='Cdc42Dat';
fileName=sprintf('%s_%s',datestr(now,formatOut),fileNameStr);
mkdir([masterRoot filesep 'Cdc42Dat']);

%% Cdc42 analysis
% This block will iterate over the experiments and cells loaded 
% in the excel sheet above. The block will determine if alignment 
% parameters have been genereated for the experiment and run the 
% aligment, if necessary. Otherwise it will load saved parameters.
% Next, the text file from the experiment will be read to pull out the
% frames before, stim initiation and frames after stim. The images will then
% be read in, and undergo our standard background subtraction and masking strategy.
% For the first frame, we will then determine the stimulated cell based on
% its location in the center of the image. The FRET ratio for the cell
% mask is then calculated by suming allYFP and allCYP pixels in the mask
% before taking the ratio rather than taking the ratio on a per pixel basis
% and taking the mean. This is to reduce the noise. 

for iCond=1;%:length(infoStruct)
    for iFolder= 1:length(infoStruct(iCond).folders) 
        parentFolder=getParentDirectory(infoStruct(iCond).folders{iFolder});
        exptype=typeExp(iCond).folders{iFolder};
        outcome=WinLose(iCond).folders(iFolder);
        [~,folderLabel]=fileparts(infoStruct(iCond).folders{iFolder});
        mkdir([masterRoot 'Scripts' filesep parentFolder]);
        mkdir([masterRoot 'Analyzed Data-Latest' filesep parentFolder]);
        %mkdir([googleDriveRoot 'Analyzed Data' filesep parentFolder]);
        
        %+++++++
        % Set Imaging Channel Names
        channel1=infoStruct(iCond).channel1;
        channel2=infoStruct(iCond).channel2;
        
        % +++++++
        %set roots
        rawDataroot = [masterRoot parentFolder];  % This should be the path to the folder containing all the data from the experiment
        scriptRoot=[masterRoot 'Scripts' filesep parentFolder];
        saveRoot=[masterRoot 'Analyzed Data-Latest' filesep parentFolder];
        %++++++++++++ 
                  
            %++++++++
            %if loop to determine if
            if exist([scriptRoot filesep 'alignment parameters pX pY.mat'])==0;
                
                %++++++++
                % file roots for aligning images
                %if computing the alignment parameters -- later we should streamline to compute alignment parameters once separately
                allSubdir=getSubdirectories(rawDataroot);
                allDataSubdir=getSubdirectories(rawDataroot,'Cell');
                allDataSubdir=natsortfiles(allDataSubdir);
                dataSubdir=allSubdir(boolRegExp(allSubdir,'^[A-Za-z][0-9][0-9]'));% This will find all the subfolders beginning with a well name
                                
                %+++++++++
                %collect images for image registration
                numPixels=1024;
                imAlign=zeros(numPixels,numPixels,2);
                imAlign2=zeros(numPixels,numPixels,2);
                temp1=zeros(numPixels,numPixels,length(allDataSubdir));
                temp2=zeros(numPixels,numPixels,length(allDataSubdir));
                for i=1:length(allDataSubdir);
                    folder=[rawDataroot filesep allDataSubdir{i}];
                    if (any(size(dir([folder '/*.tif' ]),1))) == 1
                        filenames1=getFilenames(folder, channel1);
                        filenames2=getFilenames(folder, channel2);
                        if length(filenames1) > 1 && length(filenames2) > 1
                            temp1(:,:,i)=double(imread([folder filesep filenames1{1}]));
                            temp2(:,:,i)=double(imread([folder filesep filenames2{1}]));
                        end %if filenames
                    end % if has a tif in the folder
                end
                imAlign2(:,:,1)=max(temp1,[],3);
                imAlign2(:,:,2)=max(temp2,[],3);
                                
                %compute image registration
                p0.pX=zeros(6,1);
                p0.pX(1)=-5;
                p0.pY=zeros(6,1);
                p0.pY(1)=-37;
                
                %+++++++++
                %compute image registration
                p=computeRegistrationParametersQuadFit(imAlign2,p0);
                
                %+++++++++
                %check image registration
                imgF=registerImagesFromQuadFit(imAlign2,[p.pX p.pY]);
                imgF=imageBin(imgF,1);
                showImagesMergeChannels(imgF(:,:,1),imgF(:,:,2));
                [getErrorValuesForRegisteredImages([p.pX p.pY],imgF) getErrorValuesForRegisteredImages([p.pX p.pY],imgF)]; %imstack for getError... was img0 and imgA0. Not sure what these were, I changed to imgF-GB
                pause(0.3);
                close all;
                
                %+++++++
                %save alignment parameter
                fprintf('saving alignment p');
                save([scriptRoot filesep 'alignment parameters pX pY.mat'],'p');
            else
                load([scriptRoot filesep 'alignment parameters pX pY.mat']);
            end % if alignment p do not exist
            
            %++++++++++++++++++
            %Set scale bar and image rotation matrix info
            frameDiff=2;
            tempScale=frameDiff * 3 / 60; % the temporal scale for the speed proxy calculations in min (3 sec our frame interval)
            retractionThresh=-150;
            distanceScale = 0.4389 * 2 * (10/60) *1.5;  %=0.21 um/pixel - converting between pixels and microns
            desiredLengthOfScale=25; % in um
            numPixInScaleBar=desiredLengthOfScale/distanceScale; % convert scale length in microns to pixels
            %rotation matrix
            rotationAngle=-1*-.0124;  % Multiply by negative one because we are doing the reverse transformation here
            rotationMatrix=[cos(rotationAngle) -1*sin(rotationAngle); sin(rotationAngle) cos(rotationAngle)];
            
            clear lines; clear pos0; clear linesMap; clear frapMap; clear numBefore; clear numBeforeStim; clear stimMap;

            folder=[masterRoot infoStruct(iCond).folders{iFolder}];
            
            if ~boolRegExp(infoStruct(iCond).condition,'no opsin')
                
                fprintf('Analyzing opsin experiment: %s\n', infoStruct(iCond).folders{iFolder});
                                
                %Read stage positions
                fid=fopen([folder filesep 'stage_positions.txt'],'r');
                lines=readAllLines(fid);
                fclose(fid);
                                
                %parse out the total number of images bf and af stim. Also make
                % variables for the coordinates
                linesMap=[];
                frapMap=[];
                frameLines=[];
                stimMap=[];
                for i=1:length(lines)
                    linesMap(i)=(contains(lines{i},'Initial coordinates')) | (contains(lines{i},'Stimulation coordinates'));
                    stimMap(i)=(contains(lines{i},'Stimulation coordinates'));
                    frapMap(i)=(contains(lines{i},'Frap'));
                    frameLines(i)=(contains(lines{i},'frame='));
                    numBeforeStim(i)=(contains(lines{i},'Initial coordinates'));
                end
                
                % pull out numBefore, stime Pos and Num Aft for processing
                % later
                frapMap=find(frapMap);
                numBefore=sum(numBeforeStim)-1; % last (before) frame is the first stim frame
                StimFrame=numBefore+1; 
                numAfter=sum(frameLines)-numBefore;
                
              if ~(exist([saveRoot filesep infoStruct(iCond).condition ' ' folderLabel '.avi'])==1); %determines if a movie has already been written for this current folder
                %the following loop is for movies
                if ~isempty(frapMap)
                    fprintf('frapSpt detected');
                    frapPos(1,1)=str2double(getTokens(lines{frapMap},'([0-9]+), [0-9]+'));
                    frapPos(1,2)=str2double(getTokens(lines{frapMap},'[0-9]+, ([0-9]+)'));
                    
                    %frameNum=str2double(getTokens(lines{frameLines},'frame=([\w]+)'));
                    frameInd=find(frameLines);
                    
                    %lines=lines(linesMap);
                    numFrames=sum(frameLines);
                    
                    %find the intial coordinates
                    ind1=find(linesMap,1);
                    preStimX=str2double(getTokens(lines{ind1},'Initial coordinates:\s([\-\w\.]+)'));
                    preStimY=str2double(getTokens(lines{ind1},'Initial coordinates:\s[\-\w\.]+, ([\-\w\.]+)'));
                    preStimPos=[preStimX preStimY];
                    
                    %find the number of frames before by counting instances of initial
                    %coordinates
                    if boolRegExp(parentFolder,'late')
                        numBefore=sum(boolRegExp(lines,'Initial coor'));
                    else
                        numBefore=sum(boolRegExp(lines,'Initial coor'))-1; % I am substracting 1 since the last (before) frame is the first stim frame (for late-rev not!)
                    end
                    %find the initial stimulation coordinates
                    ind2=find(stimMap,1);
                    initialStimCoorX=str2double(getTokens(lines{ind2},'Stimulation coordinates:\s([\-\w\.]+)'));
                    initialStimCoorY=str2double(getTokens(lines{ind2},'Stimulation coordinates:\s[\-\w\.]+, ([\-\w\.]+)'));
                    
                    %get X and Y coors for stim frames in uM
                    postStimPos=nan(numFrames,2);
                    pos0=nan(numFrames,2);
                    for i=1:numFrames
                        if i<numFrames
                            tokX=getTokens(lines{frameInd(i)+1},'Stimulation coordinates:\s([\-\w\.]+)');
                            tokY=getTokens(lines{frameInd(i)+1},'Stimulation coordinates:\s[\-\w\.]+, ([\-\w\.]+)');
                        else
                            tokX=[];
                            tokY=[];
                        end
                        if ~isempty(tokX)
                            postStimX=str2double(tokX);
                            postStimY=str2double(tokY);
                            postStimPos(i,1)=postStimX;
                            postStimPos(i,2)=postStimY;
                        else
                            postStimPos(i,:)=[nan nan];
                        end
                    end                   
                    
                    if ~boolRegExp(parentFolder,'late')
                        %add in the first stim coordinate
                        stimLocation=find(~isnan(postStimPos(:,1))); %to find the frame with the second stim
                        postStimPos=[postStimPos(1:stimLocation(1)-2,:); [initialStimCoorX initialStimCoorY]; postStimPos(stimLocation(1):end,:)]; % not for late reversal
                    end
                    %difference between img center and stim site
                    pos0(:,1)=postStimPos(:,1)-preStimPos(1,1); %post-pre normally
                    pos0(:,2)=-1*(postStimPos(:,2)-preStimPos(1,2)); % flipping y
                    %Convert stage shifts in microns to image shifts in pixels
                    pos1=pos0./distanceScale;
                    pos1=(rotationMatrix*pos1')';
                    pos1=round(pos1);
                    % save pos1 for unflipped images
                    pos1_n=pos1;
                    %convert the y coor to negative for flipped
                    %images
                    pos1(:,2)=pos1(:,2)*-1;
                    
                    %make frapMask image that can be then run through the im alignment. Use aligned mask for computing the translated frap spot
                    frapMask=zeros(1024,1024);
                    frapMask(frapPos(2),frapPos(1))=1; % remember that mat to image requires (Y X) org
                    frapMask(:,:,1)=frapMask;
                    frapMask(:,:,2)=frapMask(:,:,1);
                    regFrapMask=registerImagesFromQuadFit(frapMask,p);
                    regFrapMask =zeroPadIfSmaller(regFrapMask,[970 980 2]); % padding with 0s before cropping, in case the registered mask is smaller than the cropping bounds
                    regFrapMask=regFrapMask(50:970,30:980,:);
                    %flip the regFrapMask 180;
                    flippedregFrapMask=flipud(regFrapMask);
                    %now reduce the image by the bounds set above. % this is for cropping
                    %the image
                    [indX indY]=size(flippedregFrapMask(:,:,1));
                    [regFrapPos(2) regFrapPos(1)]=ind2sub([indX, indY],find(flippedregFrapMask(:,:,1)));
                    %calc the scatter location for registered ims
                    pos2=regFrapPos+pos1;
                    
                    % AH addition for unflipped: to use for checking the photobleaching due to persisent zaps 
                    %reduce the image by the bounds set above. % this is for cropping
                    %the image
                    [indX_n indY_n]=size(regFrapMask(:,:,1));
                    [regFrapPos_n(2) regFrapPos_n(1)]=ind2sub([indX_n, indY_n],find(regFrapMask(:,:,1)));
                    %calc the scatter location for registered ims
                    pos2_n=regFrapPos_n+pos1_n; % this should be the position of the FRAP spot for the non flipped masks
                                        
                    %select bounds based on cell location
                    centerX=size(flippedregFrapMask,2)/2;
                    centerY=size(flippedregFrapMask,1)/2;
                    bounds=[300 901 300 801] ; % old bounds hard coded; These bounds are applied to the resulting movie frames
                    bounds=round([centerY-300 centerY+300 centerX-250 centerX+250],0);
                    
                    %cropped frap pos
                    cropRegFrapMask=flippedregFrapMask(bounds(1):bounds(2),bounds(3):bounds(4),1);
                    [indXC indYC]=size(cropRegFrapMask);
                    [CPos(2) CPos(1)]=ind2sub([indXC, indYC],find(cropRegFrapMask));
                    posC=CPos+pos1;
                            
                end % if ~isempty(frapPos) this was the block added fr movies
              end  % for movies     
              
                %find the timepoints from timestamp for laser stims
                %read and process images
                channel3=infoStruct(iCond).channel3;
                filenames3=getFilenames(folder,channel3);
              
                %read and process images
                filenames1=getFilenames(folder,channel1);
                filenames2=getFilenames(folder,channel2);
                %makes sure that there are file names for two channels
                if ~isempty(filenames1) && ~isempty(filenames2)
                    %as an artifact of canceling, occasionally an uneven
                    % number of pictures are collected the following if
                    %loop will make the filenames1&2 the same length
                    if ~(length(filenames1)== length(filenames2))
                        if length(filenames1) > length(filenames2)
                            filenames1=filenames1(1:length(filenames2));
                        else
                            filenames2=filenames2(1:length(filenames1));
                        end
                    end
                                          
                    clear images; clear ratioImage; clear ratioImageSmooth; clear imC1; clear imR1; clear im;
                    clear Ratio_LeadingEdge_L; clear Ratio_LeadingEdge_R;
                    clear sumdiffR; clear sumdiffL;  clear YFPsmoothL; 
                    clear YFPsmoothR;  clear CFPsmoothL; clear CFPsmoothR;
                    clear YFPsmoothM; clear CFPsmoothM; 
                    
                    if boolRegExp(infoStruct(iCond).condition, 'Cdc42')
                        fprintf('\nprocessing: %s\n',infoStruct(iCond).condition);
                        %try 
                        clear maskFinal; clear tempMask; clear images; clear maskL; clear maskR;
                        clear maskC; clear ringMaskL; clear ringMaskR; clear cellMiddleMask; clear frontMaskL;
                        clear frontMaskR; clear bool; clear branchChoice; clear SumProjectedMask;
                        clear croppedmaskFinal; clear croppedYFPFinal; clear croppedCFPFinal; clear croppedratioImage;
                        
                        SumProjectedMask=0;
                        protrusionThresh=1000;
                        flagresponded=false;
                        FrameResponded=[];
                        
                        for i=1:length(filenames1)
                            clear im;
                            im(:,:,1)=double(imread([folder filesep filenames1{i}]));
                            im(:,:,1)=(im(:,:,1)-medianDark1).*donutCorrection1.*halfCorrection1;
                            im(:,:,2)=double(imread([folder filesep filenames2{i}]));
                            im(:,:,2)=(im(:,:,2)-medianDark2).*donutCorrection2.*halfCorrection2;
                            im=registerImagesFromQuadFit(im,p);
                            im = zeroPadIfSmaller(im,[970 980 2]); %padding with 0s in case the image is smaller than the cropping bounds
                            im=im(50:970,30:980,:);
                            images{i}=im;
                                                 
                            % background subtraction older sean strategy
                            imSum=sum(im,3);
                            [maskBGinitial,objectRectangles]=fretGetInitialObjectsAndBGmask(imSum); % to get accurate boxes, you need to use the updated function
                            imForSeg0=imageSubtractBackgroundWithObjectMaskingAndPadding(imSum,100,50,maskBGinitial);
                            [maskFinal{i},coors]=fretGetCellMasks_63x(imForSeg0,objectRectangles,100,im(:,:,1),im(:,:,2),[4 5]);
                            CFPFinal{i}=imageSubtractBackgroundWithObjectMaskingAndPadding(im(:,:,2),100,50,maskBGinitial);
                            YFPFinal{i}=imageSubtractBackgroundWithObjectMaskingAndPadding(im(:,:,1),100,50,maskBGinitial);
                            YFPFinal{i}=YFPFinal{i}./TKratioCorr; %we apply the ratio correction here because our strategy for computing the ratios below does not maintain the spatial info
                                                        
                            %identify the cell at the center (use for frap imaging only)
                            if i==1
                                                                
                                centerMask=zeros(1024,1024);
                                centerMask(512,512)=1;
                                centerMask(:,:,1)=centerMask;
                                centerMask(:,:,2)=centerMask(:,:,1);
                                centerMask=registerImagesFromQuadFit(centerMask,p);
                                centerMask = zeroPadIfSmaller(centerMask,[970 980 2]); %padding with 0s in case the image is smaller than the cropping bounds
                                centerMask=centerMask(50:970,30:980,:);
                                centerMask=imdilate(centerMask(:,:,1), strel('rectangle',[400 50]));
                                tempMask{i}=bwlabel(imdilate(maskFinal{i}, strel('disk',5)),8);
                                tempMask{i}=tempMask{i}==median(tempMask{i}(tempMask{i} & centerMask(:,:,1)));
                                maskFinal{i}=maskFinal{i}&tempMask{i};                  
                                
                            else
                                tempMask{i}=bwlabel(imdilate(maskFinal{i}, strel('disk',5)),8);
                                tempMask{i}=tempMask{i}==median(tempMask{i}(tempMask{i} & tempMask{i-1}));
                                maskFinal{i}=maskFinal{i}&tempMask{i};
        
                            end
                                                            
                          YFP=YFPFinal{i};
                          CFP=CFPFinal{i};
                          YFP(~maskFinal{i})=nan; % mask background from cell
                          CFP(~maskFinal{i})=nan;
                          
                          % ratio image
                          ratioImage{i}=YFP./CFP;
                          
                          croppedmaskFinal{i}=cropImMidOut(maskFinal{i},'cropsize',[151 921]);
                          croppedYFPFinal{i}=cropImMidOut(YFPFinal{i},'cropsize',[151 921]);
                          croppedCFPFinal{i}=cropImMidOut(CFPFinal{i},'cropsize',[151 921]);
                          croppedratioImage{i}=cropImMidOut(ratioImage{i},'cropsize',[151 921]);   
                          
                        end

                        Cdc42Dat.maskFinal=croppedmaskFinal;
                        Cdc42Dat.YFPFinal=croppedYFPFinal;
                        Cdc42Dat.CFPFinal=croppedCFPFinal;
                        Cdc42Dat.RatioImage=croppedratioImage;
                        Cdc42Dat.FrameStartStim=StimFrame;
                        Cdc42Dat.FramesBefore=numBefore;
                        Cdc42Dat.FramesAfter=numAfter;
                        Cdc42Dat.ExpType=exptype;
                        Cdc42Dat.WinLose=outcome;
                        Cdc42Dat.folderPathCdc42=[parentFolder filesep folderLabel];
                        save([folder filesep folderLabel],'Cdc42Dat','-v7.3');                        
                        
              end %if Cdc42 
            end %if ~isempty filenames
        end %not no opsin if        
    clear Cdc42Dat
    fprintf('num folders remaining: %i\n', length(infoStruct(iCond).folders)-iFolder);
end %iFolder
end%iCond