function channelMap=channelBranchSkeletonFunction(p)

% p(1) = branch X
% p(2) = branch Y
% p(3) = angle (slope or tilt of the incoming line)
distScale=100;
branchSpacing=27.5;

channelMap.branchPt=[p(1) p(2)];
channelMap.line1=@(p,t) repmat([p(1) p(2)]',[1 length(t)]) + t.*distScale.*angle2vector(p(3))';
channelMap.line2=@(p,t) repmat([p(1) p(2)]' - branchSpacing*angle2vector(p(3)-pi/2)',[1 length(t)]) + t.*distScale.*angle2vector(p(3))';
channelMap.line3=@(p,t) repmat([p(1) p(2)]' + branchSpacing*angle2vector(p(3)-pi/2)',[1 length(t)]) + t.*distScale.*angle2vector(p(3))';

channelMap.branchL=@(p,t) (1 - max(0,exp(t/0.11))).*channelMap.line2(p,-1*t) + max(0,exp(t/0.11)).*channelMap.line1(p,-1*t);
channelMap.branchR=@(p,t) (1 - max(0,exp(-t/0.11))).*channelMap.line3(p,t) + max(0,exp(-t/0.11)).*channelMap.line1(p,t);

channelMap.branches=@(p,t) (t<=0).*channelMap.branchL(p,t) + (t>0).*channelMap.branchR(p,t);