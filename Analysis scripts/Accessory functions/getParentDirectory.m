function path=getParentDirectory(pathIn)
% returns the parent directory of the specified path

tok=split(pathIn,filesep);

if strcmp(tok{end},'')
    tok=tok(1:(end-2));
    path=[fullfile(tok{:}) filesep];
else
    tok=tok(1:(end-1));
    path=fullfile(tok{:});
end
