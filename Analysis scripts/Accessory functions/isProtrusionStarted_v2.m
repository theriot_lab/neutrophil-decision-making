function [bool,retractionVal] = isProtrusionStarted_v2(maskStimcurrent,maskStimprevious,protrusionThresh)

bool=0;

maskEdge=maskStimcurrent-maskStimprevious;
maskBotHalf=maskStimcurrent | maskStimprevious;
d0=regionprops(maskBotHalf,'Centroid');
maskBotHalf(1:round(d0(1).Centroid(2)),:)=false;
maskEdge(~maskBotHalf)=0;
retractionVal=sum(maskEdge(:));
    if retractionVal>protrusionThresh
        bool=1;
    end
end

