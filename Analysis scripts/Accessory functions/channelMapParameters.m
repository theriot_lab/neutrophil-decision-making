function [p]=channelMapParameters(mask)
mask=imdilate(mask,strel('disk',5)); %dilating so as to avoid side branches later on
bwmask = im2bw(mask,1);  %binarize the mask
skeleton= bwskel(bwmask);
branchpoints = bwmorph(skeleton,'branchpoints');
[p(2), p(1)] = find(branchpoints,1); % find(branchpoints) that was before
%angle from the highest up pt angle from vector
%get the mask of the straight segment (y>branchpt)
straight=skeleton;
straight(p(2):end,:)=0;
straight(1:(p(2)-50),:)=0;
% %get the orientation that line segment (in degrees)
% coord=regionprops(straight,'Orientation');
% angle=coord.Orientation;  %in degrees, sth close to 88.95o
% p(3)=deg2rad(angle); %convert to radians
[sy,sx]=find(straight);
res=robustfit(sy,sx);
angle=atan(1/res(2));
p(3)=angle;
if p(3)<0
    p(3)=p(3)+pi;
end
% if the cell in the straight segment has its uropod tilted we may run into
% problems, maybe hard code the p(3) to be pi/2?
end