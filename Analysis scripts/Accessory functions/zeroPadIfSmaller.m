function mat = zeroPadIfSmaller(mat,sz1,padLocation)

if nargin<3
    padLocation='low';
end

sz0=size(mat);
sz1=max(sz1,sz0);
padsize=sz1-sz0;
for i=1:length(padsize)
    b = padsize;
    b(1:(i-1))=sz1(1:(i-1));
    b((i+1):end)=sz0((i+1):end);
    if strcmp(padLocation,'low')
        mat = cat(i,zeros(b),mat);
    else
        mat = cat(i,mat,zeros(b));
    end
end
