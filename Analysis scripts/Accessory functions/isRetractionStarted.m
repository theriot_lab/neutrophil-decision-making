function [bool,choice] = isRetractionStarted(maskLcurrent,maskLprevious,maskRcurrent,maskRprevious,retractionThresh)

maskEdgeL=maskLcurrent-maskLprevious;
maskBotHalf=maskLcurrent | maskLprevious;
d0=regionprops(maskBotHalf,'Centroid');
maskBotHalf(1:round(d0(1).Centroid(2)),:)=false;
maskEdgeL(~maskBotHalf)=0;

maskEdgeR=maskRcurrent-maskRprevious;
maskBotHalf=maskRcurrent | maskRprevious;
d0=regionprops(maskBotHalf,'Centroid');
maskBotHalf(1:round(d0(1).Centroid(2)),:)=false;
maskEdgeR(~maskBotHalf)=0;

retractionValL=sum(maskEdgeL(:));
retractionValR=sum(maskEdgeR(:));

bool=false;
choice='';
if retractionValL<retractionThresh
    bool=true;
    choice='LEFT';
elseif retractionValR<retractionThresh
    bool=true;
    choice='RIGHT';
end
