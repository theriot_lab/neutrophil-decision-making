function distMask=distanceFromMaskToLine(mask,line)
% This function uses distanceFromPointsToLine to compute the distance from
% each point in a mask to a reference line. As with the other function, the
% line should be specified as a four element vector, where the first two
% elements are the xy coordinates of a point on the line, and the last two
% elements are a vector pointing in the direction of the line.

distDifferenceThresh=15;

[ptsY,ptsX]=find(mask);
ptList=find(mask);

distList=distanceFromPointsToLine([ptsX(:) ptsY(:)],line);
distMask=nan(size(mask));
distMask(ptList)=distList;

edgePtMask=false(size(mask));
edgePtMask(line(2),line(1))=true;
distMaskFromPt=bwdistgeodesic(mask,edgePtMask,'quasi-euclidean');

distMask(distMaskFromPt-distMask>distDifferenceThresh)=nan;
