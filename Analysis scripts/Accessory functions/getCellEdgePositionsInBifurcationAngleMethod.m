function [edgeL,edgeC,edgeR, maskL,maskC,maskR] = getCellEdgePositionsInBifurcationAngleMethod(img,centroid,angle,edgeCIn,sharpParam)
if nargin < 5
    sharpParam=[4 50];
end

%7/19/2019 GB Additions: edited this function to allow it to recieve a
%masked image as the input. The function will determine if the input img is
%a raw image or a binary mask. Then move to the appropriate processing
%steps.

if ~islogical(img)

img=double(imageSubtractBackground(img(:,:,1),40,128));
imgC=img(100:900,412:612);  % Crop to keep only the center channel

% Get rid of cell pieces from the neighboring channels if they are present
% mask1=fretGetCellMasks_63x(imgC,{[1 1 200 800]}); 
% mask1=mask1-imclearborder(mask1);
% imgC(mask1>0)=0;

% Get cell mask
centerInd=round(centroid(1));
mask=fretGetCellMasks_63x(imgC,{[1 1 200 800]},2000,imgC,imgC,sharpParam);
% if necessary run imerode by 2 pixels to make sure that the cell is being
% stimulated
mask=imerode(mask,strel('disk', 2)); % 8/3/2019 AH uncommented this part
targetRegion=false(size(mask));
targetRegion(100:512,(centerInd-20-411):(centerInd+20-411))=true; % image matrix coordinates are (y,x);
targetMask=bwareafilt(targetRegion & mask>0,1);
labelMask=bwlabel(mask);
cellInd=median(labelMask(targetMask));
mask=labelMask==cellInd;

else 
    mask=img(100:900,412:612);
end

temp=false(size(img));
temp(100:900,412:612)=mask;
mask=temp;

maskC=mask & line2mask(centroid,angle,400,size(mask),2);

% In case maskC is multiple objects, pick just the biggest one
d=regionprops(maskC,'Area','PixelIdxList');
areaList=[d.Area];
[maxVal,maxInd]=max(areaList);
maskC=false(size(maskC));
maskC(d(maxInd).PixelIdxList)=true;

d=regionprops(maskC,'BoundingBox');
if nargin<4 || edgeCIn==0
    edgeC=d.BoundingBox(2)+d.BoundingBox(4);
else
    edgeC=edgeCIn;
end
mask(1:(round(edgeC)-40),:)=false;  %Crop the mask to remove the cell tail, which often causes problems

maskSplit = mask & ~maskC;

%in case of more than two objects, take just the biggest two
d=regionprops(maskSplit,'Area','PixelIdxList','Centroid');
if length(d)>2
    areaList=[d.Area];
    [areaVals,areaInd]=sort(areaList,'descend');
    maskSplit=false(size(maskSplit));
    maskSplit(d(areaInd(1)).PixelIdxList)=true;
    maskSplit(d(areaInd(2)).PixelIdxList)=true;
end

maskSplit=bwlabel(maskSplit);
if max(maskSplit(:))==1
    r=(centroid(2)-d(1).Centroid(2))/sin(angle);
    if d(1).Centroid(1) - (centroid(1)+ r*cos(angle)) > 0
        maskSplit(maskSplit==1)=maskSplit(maskSplit==1)+1;
    end
end

maskL=(maskSplit==1);  %Objects should be labeled from left to right, so object 1 should be the left mask
d=regionprops(maskL,'BoundingBox');
if ~isempty(d)
    edgeL=d.BoundingBox(2)+d.BoundingBox(4);
else
    edgeL=edgeC;
end

maskR=(maskSplit==2);
d=regionprops(maskR,'BoundingBox');
if ~isempty(d)
    edgeR=d.BoundingBox(2)+d.BoundingBox(4);
else
    edgeR=edgeC;
end
