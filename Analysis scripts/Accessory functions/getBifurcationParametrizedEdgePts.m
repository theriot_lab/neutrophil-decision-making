function [edgePtL,edgePtR,edgeMaskL,edgeMaskR]=getBifurcationParametrizedEdgePts(mask,channelMap,p,numPix)

t=-3:0.01:3;
pointsAll0=channelMap.branches(p,t);
pointsAll=round(pointsAll0);
boolvals=false(size(t));
for i=1:length(t)
    boolvals(i)=mask(pointsAll(2,i),pointsAll(1,i));
end

edgePtL=pointsAll(:,find(boolvals,1,'first'));
pt1=pointsAll0(:,find(boolvals,1,'first'));
pt2=pointsAll0(:,find(boolvals,1,'first')+1);
v1=pt2-pt1;
lineL=[edgePtL(1) edgePtL(2) -1*v1(2) v1(1)];

edgePtR=pointsAll(:,find(boolvals,1,'last'));
pt1=pointsAll0(:,find(boolvals,1,'last'));
pt2=pointsAll0(:,find(boolvals,1,'last')+1);
v1=pt2-pt1;
lineR=[edgePtR(1) edgePtR(2) -1*v1(2) v1(1)];


distMaskL=distanceFromMaskToLine(mask,lineL);
pts=find(~isnan(distMaskL));
mat=[pts(:) distMaskL(pts(:))];
mat=sortrows(mat,2,'ascend');
pixInd=mat(1:numPix,1);
edgeMaskL=false(size(mask));
edgeMaskL(pixInd)=true;

distMaskR=distanceFromMaskToLine(mask,lineR);
pts=find(~isnan(distMaskR));
mat=[pts(:) distMaskR(pts(:))];
mat=sortrows(mat,2,'ascend');
pixInd=mat(1:numPix,1);
edgeMaskR=false(size(mask));
edgeMaskR(pixInd)=true;
