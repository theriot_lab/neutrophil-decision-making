function [arrSmoothedXYCoords, arrInterpolatedXYCoords] =  fnSmoothCurve(argXYCoords, optSmoothing, optIsTrack, optNumInterpolations)% Takes an N x 2 matrix of XY coordinates and returns a smoothed curve with
% new coordinates using spline fitting
%
% Adapted from cntrd2dispvec() originally developed by Tony Tsai
 %
 % Arguments:
 %
 %   argXYCoords          = an N x 2 matrix of XY coordinates
 %   optSmoothing         = smoothing parameter P that ranges from 0 -> 1
 %                          (default = 0.5), where 0 fits a straight line,
 %                          and 1 gives a total fit (optional)
 %   optIsTrack           = the plotted curve will use IJ axis mode
 %                          (origin is at the top-left corner of the
 %                          plot), useful for plotting cell tracks.
 %                          Default = 0 -> use XY axis mode (optional)
 %   optNumInterpolations = number of interpolation points to extract the
 %                          smoothed curve's values from
 %
 % Author: Amalia Hadjitheodorou (4/2016)
 %
 % Modified:
 %   - Amalia Hadjitheodorou (8/2016): Changed from fnSmoothTrack() to
 %                          fnSmoothCurve() to generalize the usage of
 %                          this function
 
 blnDEBUG = 0;
 
 if (~exist('optSmoothing', 'var') || optSmoothing < 0 || optSmoothing > 1)
   optSmoothing = 0.5;
 end
 
 if (~exist('optIsTrack', 'var'))
   optIsTrack = 0;
 end
 
 intNumDataPoints = size(argXYCoords, 1);  % Total number of data points in the curve
 
 % Perform a smooth spline fit on the given track and return a piecewise
 % polynomial (PP). If no smoothing is required, then the functions
 % csaps() and spline() give similar results
 structPPSpline = csaps([0:intNumDataPoints - 1], argXYCoords', optSmoothing);
 %structPPSpline = spline([0:intNumDataPoints - 1], argXYCoords');
 
 arrSmoothedXYCoords = ppval(structPPSpline, [0:intNumDataPoints - 1])';
 
 if (exist('optNumInterpolations', 'var'))
   arrInterpolatedXYCoords = ppval(structPPSpline, linspace(0, intNumDataPoints - 1, optNumInterpolations))';
 end
 
 if (blnDEBUG)
   figure('Name', ['Smoothed Curve (optSmoothing = ', num2str(optSmoothing), ')']); hold on;
   plot(arrSmoothedXYCoords(:, 1), arrSmoothedXYCoords(:, 2), '--*');  % Plot the smoothed curve and dots
   plot(argXYCoords(:, 1), argXYCoords(:, 2), 'xr');  % Plot the original data points
   text(argXYCoords(:, 1), argXYCoords(:, 2), cellstr(num2str([1:intNumDataPoints]')));  % Label the data points in chronological order
   
   if (exist('optNumInterpolations', 'var'))
     plot(arrInterpolatedXYCoords(:, 1), arrInterpolatedXYCoords(:, 2), 'ob');  % Plot the interpolated dots
     text(arrInterpolatedXYCoords(:, 1), arrInterpolatedXYCoords(:, 2), cellstr(num2str([1:optNumInterpolations]')), 'Color', 'm');  % Label the data points in chronological order
   end
   
   % Plot the curve using IJ axis mode if this is a cell track
   if (optIsTrack)
     axis('equal', 'ij');
   end
 end
   
end