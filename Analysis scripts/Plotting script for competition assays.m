%% Script to read in the analyzed .mat files and produce plots for Fig 4 
% this script was used to compute and plot migration and signaling
% measuments pertaining to competition datasets

%% Directories
masterRoot='/Volumes/LaCie/UC_Davis_Collab/Data/';
% Determine where your m-file's foclder is.
folder = '/Volumes/LaCie/UC_Davis_Collab/Code';
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));

%% Read in curated files

load('/Volumes/LaCie/UC_Davis_Collab/Data/Cdc42DatContComp/Cdc42Dat-Competition-nostimulation'); % no stimulation competition data
%load('/Volumes/LaCie/UC_Davis_Collab/Data/Cdc42DatContComp/Cdc42Dat-Continuous-Competition'); % continuous competition data

%% Correction for Cdc42 activity at the front: will need to do that for back

% When the front is considered as 800 pixels closest to front edge the fitting in straight channel data gives: Y = -0.0004898*X + 1.000
% When the front is considered as 400 pixels closest to front edge the fitting in straight channel data gives: Y = -0.0005231*X + 1.000
% When the front is considered as 200 pixels closest to front edge the fitting in straight channel data gives: Y = -0.0005541*X + 1.000

%% Load in data in auxialiary variables to later plot
N = length(Cdc42CompDat);
tempScale= 3 / 60; % 3 sec time interval, dividing by 60sec to turn the speed into min
distanceScale = 0.4389 * 2 * (10/60) *1.5;

for iFolder=1:N
    
    auxiliary=Cdc42CompDat(iFolder).maskFinal;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EntireCellMask(iFolder,1:length(Cdc42CompDat(iFolder).maskFinal))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).edgeMaskR400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EdgeMaskR400(iFolder,1:length(Cdc42CompDat(iFolder).edgeMaskR400))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).edgeMaskL400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EdgeMaskL400(iFolder,1:length(Cdc42CompDat(iFolder).edgeMaskL400))=auxiliary; % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_FrontL400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FrontLeftCdc42(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_FrontL400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_FrontR400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    FrontRightCdc42(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_FrontR400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_Back400;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    BackCdc42(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_Back400))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_Cell;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    EntireCellCdc42(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_Cell))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_SkeletonLength;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    SkeletonLength(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_SkeletonLength))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_Elongation;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    MajorAxis(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_Elongation))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    auxiliary=Cdc42CompDat(iFolder).Ratio_AreaTotal;
    auxiliary(cellfun(@isempty, auxiliary))={nan};
    CellArea(iFolder,1:length(Cdc42CompDat(iFolder).Ratio_AreaTotal))=cell2mat(auxiliary); % this will be a cell array structure
    clear auxiliary
    
    FramesBeforeStim(iFolder)=Cdc42CompDat(iFolder).framesNoStimed;
    FramesStimed(iFolder)=Cdc42CompDat(iFolder).framesStimed;
    FrameContact(iFolder)=Cdc42CompDat(iFolder).framesContact;
    ExpType{iFolder}=Cdc42CompDat(iFolder).ExpType;
    WinLose(iFolder)=Cdc42CompDat(iFolder).WinLose;
end

%% Find frame when stimulation would have started if continuous/5pulse assay was employed
% In cells that received the continuous stim and the 5pulse stim the stim
% started when the two fronts extended more than 10px from the apex of the
% obstacle, whereas for later when more than 30px
sharpParam=[4 5];
frameDiff=3;
for iFolder=1:N
    numBefore = 0;
    conditionFlag=false;
    edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
    PaddedMask{iFolder,1}=false([921 951]);
    PaddedMask{iFolder,1}(:,400:550)=EntireCellMask{iFolder,1}; 
    try
        [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(PaddedMask{iFolder,1});
        for numframe=2:length(find(~cellfun(@isempty,EntireCellMask(iFolder,:)))) 
            numBefore = numBefore +1;
            PaddedMask{iFolder,numframe}=false([921 951]);
            PaddedMask{iFolder,numframe}(:,400:550)=EntireCellMask{iFolder,numframe};
            [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                = getCellEdgePositionsInBifurcationAngleMethod(PaddedMask{iFolder,numframe},initialCellCentroid,channelAngle, 0 ,sharpParam);
            if numBefore > frameDiff & (edgeL(numBefore)-edgeC(numBefore) > 10) & (edgeR(numBefore)-edgeC(numBefore) > 10) % 10 for continuous, change both 30 for late competition
                FrameHypotheticalStim(iFolder)=numframe;
                break;
            end
        end
    catch
        FrameHypotheticalStim(iFolder)=nan;
    end
    clear PaddedMask
end

%% Find frame when big retraction begins
% Compute the frame at which the cell started retracting one edge, as well
% as which edge that was
sharpParam=[4 5];
frameDiff=2;
retractionThresh=-150; %threshold to determine if a big enough retraction is occurring.
for iFolder=1:N
    numBefore = 0;
    edgeL=[]; edgeC=[]; edgeR=[]; maskL={}; maskC={}; maskR={};
    PaddedMask{iFolder,1}=false([921 951]);
    % in fact I want to start this analysis after contact so for the first
    % frame I will use the index=FrameContact(iFolder) instead of 1
    if ~isnan(FrameContact(iFolder))
        PaddedMask{iFolder,1}(:,400:550)=EntireCellMask{iFolder,FrameContact(iFolder)};
        try
            [initialCellCentroid,channelAngle] = getCentroidAndAngleOfCellCenterInChannel(PaddedMask{iFolder,1});
            conditionFlag=true;
            for numframe=(FrameContact(iFolder)+1):length(find(~cellfun(@isempty,EntireCellMask(iFolder,:))))
                numBefore = numBefore +1;
                PaddedMask{iFolder,numBefore}=false([921 951]);
                PaddedMask{iFolder,numBefore}(:,400:550)=EntireCellMask{iFolder,numframe};
                [edgeL(numBefore), edgeC(numBefore), edgeR(numBefore), maskL{numBefore}, maskC{numBefore}, maskR{numBefore}]...
                    = getCellEdgePositionsInBifurcationAngleMethod(PaddedMask{iFolder,numBefore},initialCellCentroid,channelAngle,0,sharpParam);
                if numBefore-frameDiff>0 & conditionFlag
                    [bool,branchChoice] = isRetractionStarted(maskL{numBefore},maskL{numBefore-frameDiff},maskR{numBefore},maskR{numBefore-frameDiff},retractionThresh);
                    %imagesc(imfuse(maskR{numBefore},maskL{numBefore}))
                    %pause(0.1)
                else
                    bool=false;
                end
                if bool
                    FrameRetractionStarted(iFolder)=numframe;
                    % compute target location
                    if strcmp(branchChoice,'LEFT')  
                        RetractingSide{iFolder}='LEFT';
                        break;
                    else
                        RetractingSide{iFolder}='RIGHT';
                        break;
                    end
                end
            end
        catch
            RetractingSide{iFolder}=nan;
            FrameRetractionStarted(iFolder)=nan;
        end
    else
        RetractingSide{iFolder}=nan;
        FrameRetractionStarted(iFolder)=nan;
    end
    clear PaddedMask
end

%% Duration computation--block to generate Fig. 4I
% this block was used to generate data in Fig 4I, keeping only data that
% would have met the criterion for late stimulation (eg 30px extension from apex)
for i=1:N
    if ~isnan(FrameRetractionStarted(i)) & ~isnan(FrameContact(i)) & ~isnan(FramesBeforeStim(i)) & ~isnan(FrameHypotheticalStim(i)) & FrameHypotheticalStim(i)>FrameContact(i) & FrameHypotheticalStim(i)<FrameRetractionStarted(i)
        ContactToRetraction(i)=(FrameRetractionStarted(i)-FrameContact(i))*3; % frame interval is 3 s
        StimToRetraction(i)=(FrameRetractionStarted(i)-(FramesBeforeStim(i)+1))*3; % frame interval is 3 s
    else
        ContactToRetraction(i)=nan;
        StimToRetraction(i)=nan;
    end
    if ContactToRetraction(i)<0 | StimToRetraction(i)<0
        ContactToRetraction(i)=nan;
        StimToRetraction(i)=nan;
    end
end

%% Duration for no stimulation experiments--block to generate Fig. 4I
% this block was used to generate data in Fig 4I, keeping only data that
% would have met the criterion for late stimulation (eg 30px extension from apex)
for i=1:N
    if ~isnan(FrameRetractionStarted(i)) & ~isnan(FrameContact(i))  & ~isnan(FrameHypotheticalStim(i)) & (FrameHypotheticalStim(i))>0 & FrameHypotheticalStim(i)>FrameContact(i) & FrameHypotheticalStim(i)<FrameRetractionStarted(i)
        ContactToRetraction(i)=(FrameRetractionStarted(i)-FrameContact(i))*3; % frame interval is 3 s
    else
        ContactToRetraction(i)=nan;
    end
    if ContactToRetraction(i)<0
        ContactToRetraction(i)=nan;
    end
end

%% Speed calculation leveraging protrusion/retraction areas

distanceScale = 0.4389 * 2 * (10/60) *1.5;
tempScale= 3 / 60; %temporal scale for the speed calculations in min (frame interval=3s)
frameDiff=2;
FrameDuration=ContactToRetraction./3;
timepoints=round(nanmean(ContactToRetraction)./3);

%compute retraction/protrusion areas
for iFolder=1:N % all movies
    clear LeftMaskDiff RightMaskDiff
    for i=1+frameDiff:find(~cellfun(@isempty,EdgeMaskL400(iFolder,:)),1,'last')
        if ~isnan(EdgeMaskL400{iFolder,i}) & ~isnan(EdgeMaskL400{iFolder,i-frameDiff}) & ~isnan(EdgeMaskR400{iFolder,i}) & ~isnan(EdgeMaskR400{iFolder,i-frameDiff})
            try
                [yes LeftMaskDiff(i)]=isProtrusionStarted_v2(EdgeMaskL400{iFolder,i},EdgeMaskL400{iFolder,i-frameDiff},150);
                [yes RightMaskDiff(i)]=isProtrusionStarted_v2(EdgeMaskR400{iFolder,i},EdgeMaskR400{iFolder,i-frameDiff},150);
                
            catch
                LeftMaskDiff(i)=nan;
                RightMaskDiff(i)=nan;
            end
        end
    end
    LeftAreaDiff{iFolder}=LeftMaskDiff;
    RightAreaDiff{iFolder}=RightMaskDiff;
end

%% Plot speed data only for left turning cells--block to generate Figs. 4A, 4D
% For competition stimulation assays we were always stimulating the left fronts so
% select left turning cells that received stimulation and turned left,
% among these some are spontaneous and some induced left turning cells

LeftTurningIndices=find(WinLose==1); % from these some are responsive and the rest would have gone left irrespective of the stimulation
clear interpolated_StimedWinnersMaskDiff interpolated_NonStimedLosersMaskDiff
k=0;
for iFolder=LeftTurningIndices
    k=k+1;
    clear n_matrix1 n_matrix2 n_I1matrix1 n_I2matrix1 n_I1matrix2 n_I2matrix2 oldvalsStimedMaskDiff oldvalsNonStimedMaskDiff
    try
        oldvalsStimedMaskDiff=LeftAreaDiff{iFolder}(FrameContact(iFolder):FrameRetractionStarted(iFolder)); % relevant time of interest to interpolate
        oldvalsNonStimedMaskDiff=RightAreaDiff{iFolder}(FrameContact(iFolder):FrameRetractionStarted(iFolder));
        
        n_matrix1=[[1:FrameDuration(iFolder)+1];oldvalsStimedMaskDiff]';
        [n_I1matrix1,n_I2matrix1]=fnSmoothCurve(n_matrix1,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_StimedWinnersMaskDiff(k,:)=n_I2matrix1(:,2);
        
        n_matrix2=[[1:FrameDuration(iFolder)+1];oldvalsNonStimedMaskDiff]';
        [n_I1matrix2,n_I2matrix2]=fnSmoothCurve(n_matrix2,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_NonStimedLosersMaskDiff(k,:)=n_I2matrix2(:,2);
    catch
        interpolated_StimedWinnersMaskDiff(k,:)=nan;
        interpolated_NonStimedLosersMaskDiff(k,:)=nan;
    end
end

% Clean up by removing any zero values
interpolated_StimedWinnersMaskDiff(interpolated_StimedWinnersMaskDiff==0)=nan;
interpolated_NonStimedLosersMaskDiff(interpolated_NonStimedLosersMaskDiff==0)=nan;

interpolated_StimedWinnersMaskDiff=interpolated_StimedWinnersMaskDiff((all((~isnan(interpolated_StimedWinnersMaskDiff)),2)),:);
interpolated_NonStimedLosersMaskDiff=interpolated_NonStimedLosersMaskDiff((all((~isnan(interpolated_NonStimedLosersMaskDiff)),2)),:);

% Unit conversion
interpolated_StimedWinnersMaskDiff=interpolated_StimedWinnersMaskDiff.*(distanceScale*distanceScale/(frameDiff*tempScale));
interpolated_NonStimedLosersMaskDiff=interpolated_NonStimedLosersMaskDiff.*(distanceScale*distanceScale/(frameDiff*tempScale));

N2=length(interpolated_NonStimedLosersMaskDiff); % Correct and compute the non nan rows
time=(0:(timepoints-1))./(timepoints-1); % This will be our interpolated time relevant to the big retraction

% Compute and plot
mean_stimwinnersedgespeed = nanmean(interpolated_StimedWinnersMaskDiff);
std_stimwinnersedgespeed = nanstd(interpolated_StimedWinnersMaskDiff);
sterr_stimwinnersedgespeed=std_stimwinnersedgespeed./sqrt(N2);
mean_nonstimloseredgespeed = nanmean(interpolated_NonStimedLosersMaskDiff);
std_nonstimloseredgespeed = nanstd(interpolated_NonStimedLosersMaskDiff);
sterr_nonstimloseredgespeed=std_nonstimloseredgespeed./sqrt(N2);

figure()
boundedline(time,mean_stimwinnersedgespeed,std_stimwinnersedgespeed,'-b','transparency', 0.1)
hold on
plot(time,mean_stimwinnersedgespeed,'-b','LineWidth',2)
hold on
boundedline(time,mean_nonstimloseredgespeed ,std_nonstimloseredgespeed,'-r','transparency', 0.1)
hold on
plot(time,mean_nonstimloseredgespeed,'-r','LineWidth',2)
hold on
errorbar(time,mean_stimwinnersedgespeed,sterr_stimwinnersedgespeed,'b')
hold on
errorbar(time,mean_nonstimloseredgespeed,sterr_nonstimloseredgespeed,'r')
ylabel('area speed (microns^2/min)')
xlabel('interpolated time between contact and big retraction')
set(gca,'FontSize',20)
ylim([-150 170])

%% Change in area and cell length--block to generate Figs. 4C, 4F

FrameDuration=ContactToRetraction./3;
timepoints=round(nanmean(ContactToRetraction)./3); 
clear interpolated_CellArea interpolated_SkeletonLength

for iFolder=1:N % all movies
    clear n_matrix1 n_matrix2 n_I1matrix1 n_I2matrix1 n_I1matrix2 n_I2matrix2 oldvalsStimedMaskDiff oldvalsNonStimedMaskDiff
    try
        oldvalsCellArea=CellArea(iFolder,FrameContact(iFolder):FrameRetractionStarted(iFolder))./CellArea(iFolder,FrameContact(iFolder)); % relevant time of interest to interpolate, and normalization based on contact area
        oldvalsSkeletonLength=SkeletonLength(iFolder,FrameContact(iFolder):FrameRetractionStarted(iFolder))./SkeletonLength(iFolder,FrameContact(iFolder));
        
        n_matrix1=[[1:FrameDuration(iFolder)+1];oldvalsCellArea]';
        [n_I1matrix1,n_I2matrix1]=fnSmoothCurve(n_matrix1,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_CellArea(iFolder,:)=n_I2matrix1(:,2);
        
        n_matrix2=[[1:FrameDuration(iFolder)+1];oldvalsSkeletonLength]';
        [n_I1matrix2,n_I2matrix2]=fnSmoothCurve(n_matrix2,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_SkeletonLength(iFolder,:)=n_I2matrix2(:,2);
        continue
        interpolated_CellArea(iFolder,:)=nan;
        interpolated_SkeletonLength(iFolder,:)=nan;
    end
end

% Clean up the interpolated values to remove any zero values
interpolated_CellArea(interpolated_CellArea==0)=nan;
interpolated_SkeletonLength(interpolated_SkeletonLength==0)=nan;

interpolated_CellArea=interpolated_CellArea((all((~isnan(interpolated_CellArea)),2)),:);
interpolated_SkeletonLength=interpolated_SkeletonLength((all((~isnan(interpolated_SkeletonLength)),2)),:);

N2=length(interpolated_CellArea); % Correct and compute the non nan rows
time=(0:(timepoints-1))./(timepoints-1); % This will be our interpolated time relevant to the big retraction

% Compute and plot 
mean_CellArea = nanmean(interpolated_CellArea);
std_CellArea = nanstd(interpolated_CellArea);
sterr_CellArea=std_CellArea./sqrt(N2);
mean_SkeletonLength = nanmean(interpolated_SkeletonLength);
std_SkeletonLength = nanstd(interpolated_SkeletonLength);
sterr_SkeletonLength=std_SkeletonLength./sqrt(N2);

figure()
hold on
errorbar(time,mean_SkeletonLength,sterr_SkeletonLength,'m')
hold on
ylabel('Relative change in cell length')
hold on
errorbar(time,mean_CellArea,sterr_CellArea,'g')
hold on
ylabel('relative change in cell length and area')
xlabel('interpolated time between contact and retraction')
set(gca,'FontSize',20)
ylim([0.95 1.28])

%% Correct photobleaching induced by imaging

%slopef= -0.0004898; % for 800 points masks
slopef=-0.0005231; %for 400
%slopef= -0.0005541 %for 200
x=[0:300];
yfit = slopef*x+1; %this is a linear correction for photobleaching at the cell front

for iFolder=1:N
    for numframe=1:length(find(~cellfun(@isempty,EntireCellMask(iFolder,:))))
        try
            CorrectedFrontLeftCdc42(iFolder,numframe)=FrontLeftCdc42(iFolder,numframe)./yfit(numframe);
            CorrectedFrontRightCdc42(iFolder,numframe)=FrontRightCdc42(iFolder,numframe)./yfit(numframe);
        catch
            CorrectedFrontLeftCdc42(iFolder,numframe)=nan;
            CorrectedFrontRightCdc42(iFolder,numframe)=nan;
        end
    end
end

%%  Plot Cdc42 activity at each front for stimulated cells that won (aka turned left)--block to generate Figs. 4B, 4E
% Plotting winning side vs losing side only when the winning side was
% stimulated, for competition assays we were always stimulating the left fronts so
% select left turning cells that received stimulation and turned left

LeftTurningIndices=find(WinLose==1); % from these some are responsive and the rest would have gone left irrespective of the stim

clear interpolated_StimedWinners interpolated_NonStimedLosers
k=0;
for iFolder=LeftTurningIndices
    k=k+1;
    clear n_matrix1 n_matrix2 n_I1matrix1 n_I2matrix1 n_I1matrix2 n_I2matrix2 oldvalsStimedWinners oldvalsNonStimedLosers
    try
        oldvalsStimedWinners=CorrectedFrontLeftCdc42(iFolder,FrameContact(iFolder):FrameRetractionStarted(iFolder)); % relevant time of interest to interpolate
        oldvalsNonStimedLosers=CorrectedFrontRightCdc42(iFolder,FrameContact(iFolder):FrameRetractionStarted(iFolder));
        
        n_matrix1=[[1:FrameDuration(iFolder)+1];oldvalsStimedWinners]';
        [n_I1matrix1,n_I2matrix1]=fnSmoothCurve(n_matrix1,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_StimedWinners(k,:)=n_I2matrix1(:,2);
        
        n_matrix2=[[1:FrameDuration(iFolder)+1];oldvalsNonStimedLosers]';
        [n_I1matrix2,n_I2matrix2]=fnSmoothCurve(n_matrix2,0.5,0,timepoints); %timepoints: interpolated pts (mpoints)
        interpolated_NonStimedLosers(k,:)=n_I2matrix2(:,2);
    catch
        interpolated_StimedWinners(k,:)=nan;
        interpolated_NonStimedLosers(k,:)=nan;
    end
end

% Clean up to remove any zero values
interpolated_StimedWinners(interpolated_StimedWinners==0)=nan;
interpolated_NonStimedLosers(interpolated_NonStimedLosers==0)=nan;

interpolated_StimedWinners=interpolated_StimedWinners((all((~isnan(interpolated_StimedWinners)),2)),:);
interpolated_NonStimedLosers=interpolated_NonStimedLosers((all((~isnan(interpolated_NonStimedLosers)),2)),:);

time=-(timepoints-1):0; % This will be our interpolated time relevant to the big retraction
N2=length(interpolated_StimedWinners); % Correct and compute the non nan rows
time=(0:(timepoints-1))./(timepoints-1); % This will be our interpolated time relevant to the big retraction

% Compute and plot 
mean_stimedwinnersCdc42 = nanmean(interpolated_StimedWinners);
std_stimedwinnersCdc42 = nanstd(interpolated_StimedWinners);
sterr_stimedwinnersCdc42=std_stimedwinnersCdc42./sqrt(N2);
mean_nonstimedlosersCdc42 = nanmean(interpolated_NonStimedLosers);
std_nonstimedlosersCdc42 = nanstd(interpolated_NonStimedLosers);
sterr_nonstimedlosersCdc42=std_nonstimedlosersCdc42./sqrt(N2);

figure()
boundedline(time,mean_stimedwinnersCdc42,std_stimedwinnersCdc42,'-b','transparency', 0.1)
hold on
plot(time,mean_stimedwinnersCdc42,'-b','LineWidth',2)
hold on
boundedline(time,mean_nonstimedlosersCdc42,std_nonstimedlosersCdc42,'-r','transparency', 0.1)
hold on
plot(time,mean_nonstimedlosersCdc42,'-r','LineWidth',2)
hold on
errorbar(time,mean_stimedwinnersCdc42,sterr_stimedwinnersCdc42,'b')
hold on
errorbar(time,mean_nonstimedlosersCdc42,sterr_nonstimedlosersCdc42,'r')
ylabel('Cdc42 activity')
xlabel('interpolated time between contact and big retraction')
set(gca,'FontSize',20)
ylim([0.98 1.15])

%% Stratify the sides based on whether they won or lost 
% irrespective to whether cells turned left or right
% WinLose=1 denotes left turning cells, and WinLose=0 right turning cells
k=0;
for iFolder=1:N
    if WinLose(iFolder)==1
        WinningAreaDiff{iFolder}=LeftAreaDiff{iFolder};
        LosingAreaDiff{iFolder}=RightAreaDiff{iFolder};
        WinningSideCdc42(iFolder,:)=CorrectedFrontLeftCdc42(iFolder,:);
        LosingSideCdc42(iFolder,:)=CorrectedFrontRightCdc42(iFolder,:);
    else
        WinningAreaDiff{iFolder}=RightAreaDiff{iFolder};
        LosingAreaDiff{iFolder}=LeftAreaDiff{iFolder};
        WinningSideCdc42(iFolder,:)=CorrectedFrontRightCdc42(iFolder,:);
        LosingSideCdc42(iFolder,:)=CorrectedFrontLeftCdc42(iFolder,:);
    end
end

%% Connect cell stretching with degree of Cdc42 activity sensor fold-change--block to generate Figs. 4G
% Scatter plot where x the cell state (length change) close to when big
% retraction happens, and y the change in Cdc42 activity sensor 

% plotting with regards to winning side
FramesStimBegan=FramesBeforeStim+1;
for iFolder=1:N
    try
        ChangeInLength(iFolder)=mean(SkeletonLength(iFolder,(FrameRetractionStarted(iFolder)-3:FrameRetractionStarted(iFolder)-2)))./SkeletonLength(iFolder,FramesStimBegan(iFolder));
        ChangeInCdc42Winning(iFolder)=mean(WinningSideCdc42(iFolder,(FrameRetractionStarted(iFolder)-3:FrameRetractionStarted(iFolder)-2)))./WinningSideCdc42(iFolder,FramesStimBegan(iFolder));
    catch
        ChangeInLength(iFolder)=nan;
        ChangeInCdc42Winning(iFolder)=nan;
    end
end

figure()
hold on
scatter(ChangeInLength,ChangeInCdc42Winning,'c', 'filled')
xlabel('normalized cell length change')
ylabel('change in Cdc42 activity at winning side')
xlim([0.5 1.5])
ylim([0.85 1.05])

%% Temporal cross-correlation between cell stretching and Cdc42 activity difference--block to generate Figs. 4H

FramesStimBegan=FramesBeforeStim+1;
countnonnan=0;

for iFolder=1:N
    clear RelativeSkeletonLength UseWinningSideCdc42 UseLosingSideCdc42 UseCdc42Diff 
    if FrameRetractionStarted(iFolder)-FramesStimBegan(iFolder)>10
    % cell length
    RelativeSkeletonLength=SkeletonLength(iFolder,FramesStimBegan(iFolder):FrameRetractionStarted(iFolder))./SkeletonLength(iFolder,FramesStimBegan(iFolder));
    % winning front Cdc42
    UseWinningSideCdc42=WinningSideCdc42(iFolder,FramesStimBegan(iFolder):FrameRetractionStarted(iFolder));
    % losing front Cdc42
    UseLosingSideCdc42=LosingSideCdc42(iFolder,FramesStimBegan(iFolder):FrameRetractionStarted(iFolder));
    % difference between Cdc42 at two fronts
    UseCdc42Diff=abs(UseWinningSideCdc42-UseLosingSideCdc42);
    
    v1=smooth(RelativeSkeletonLength,0.1,'loess');
    v2=smooth(UseCdc42Diff,0.1,'loess');
    
    lags=[-10:10];
    CorVec2=zeros(size(lags));
    for i=1:length(lags)
        if lags(i)<=0
            v1a=v1((1-lags(i)):end);
            v2a=v2(1:length(v1a));
        else
            v2a=v2((1+lags(i)):end);
            v1a=v1(1:length(v2a));
        end
        CorVec2(i)=myNanCorrcoef(v1a,v2a); len2(i)=length(v1a);  % myNanCorrcoef computes Pearson's correlation coefficients ignoring NaN values
    end
        CorVec_keep{iFolder}=CorVec2;
        countnonnan=countnonnan+1;
    else
        CorVec_keep{iFolder}=nan;
    end
end

for iFolder=1:N
    CrossCorrelation(iFolder,:)=CorVec_keep{iFolder};
end

% plot cross-correlation
mean_CrossCorrelation = nanmean(CrossCorrelation);
std_CrossCorrelation = nanstd(CrossCorrelation);
sterr_CrossCorrelation=std_CrossCorrelation./sqrt(N);

figure()
errorbar(lags*(-1)*3,mean_CrossCorrelation,sterr_CrossCorrelation,'k')
ylabel('Pearsons Correlation')
xlabel('time offeset (s)')
set(gca,'FontSize',20)