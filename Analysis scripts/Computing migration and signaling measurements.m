%% Script to read in the analyzed .mat files and compute additional metrics in a new .mat to then plot
% Step 1: Load in the data
% Step 2: Save folderPathCdc42, framesNoStimed, framesStimed, ExpType,
% WinLose, maskFinal (these have been extracted before)
% Step 3: Compute the functional form of the bifurcation channel to be used
% for edge point calculation and edge masks
% Step 4: Compute edge masks for different penetration depths eg. 200, 400,
% 800 px as well as Cdc42 activity in these areas
% Step 5: Compute the sum of the Y shaped skeleton (SkeletonLength) and the 
% long axis as two proxies for cell elongation and the cell area 
% Step 6: Save the computed parameters

%% Directories
masterRoot='/Volumes/LaCie/UC_Davis_Collab/Data/';
% Determine where your m-file's folder is.
folder = '/Volumes/LaCie/UC_Davis_Collab/Code';
% Add that folder plus all subfolders to the path.
addpath(genpath(folder));
%% Use excel sheet to import good cell folders
[~,sheets]=xlsfinfo([masterRoot 'AllBifurcationData.xlsx']);
clear infoStruct;
for i=1:length(sheets)
    [~,~, raw]= xlsread([masterRoot 'AllBifurcationData.xlsx'],i);
    infoStruct(i).condition=sheets{i};
    cellNums=cell2mat(raw(2:end,1));
    tempFolders=raw(2:end,2);
    infoStruct(i).folders=tempFolders(cellNums>0);
    tempWinLose=cell2mat(raw(2:end,3));
    tempTypeExp=(raw(2:end,4));
    WinLose(i).folders=tempWinLose(cellNums>0);
    typeExp(i).folders=tempTypeExp(cellNums>0);
    if boolRegExp(sheets(i),'Cdc42')
        infoStruct(i).channel1='TomKat-1';
        infoStruct(i).channel2='TomKat-2';
    elseif boolRegExp(sheets(i),'Myosin')
        infoStruct(i).channel1='PIP3Dual-1';
        infoStruct(i).channel2='PIP3Dual-2';
    end
    infoStruct(i).channel3='FRAP'; %to be used for extracting the timestaps of the Frap stims
end

%% save Data in a matfile for future use
formatOut='yyyy-mm-dd';
fileNameStr='Cdc42DatContComp';
fileName=sprintf('%s_%s',datestr(now,formatOut),fileNameStr);
mkdir([masterRoot filesep 'Cdc42DatContComp']);

%%
for iCond=1%:length(infoStruct)
    for iFolder=1:length(infoStruct(iCond).folders)
        parentFolder=getParentDirectory(infoStruct(iCond).folders{iFolder});
        exptype=typeExp(iCond).folders{iFolder};
        outcome=WinLose(iCond).folders(iFolder);
        [~,folderLabel]=fileparts(infoStruct(iCond).folders{iFolder});
        
        %+++++++
        % Set Imaging Channel Names
        channel1=infoStruct(iCond).channel1;
        channel2=infoStruct(iCond).channel2;
        
        % +++++++
        %set roots
        rawDataroot = [masterRoot parentFolder];  % This should be the path to the folder containing all the data from the experiment
        scriptRoot=[masterRoot 'Scripts' filesep parentFolder];
        saveRoot=[masterRoot];
        
        % read in the saved .mat with the calculated parameters-> Need to
        % add a catch correction if that doesn't exists
        load([rawDataroot filesep folderLabel filesep num2str(folderLabel) '.mat']);
        
        %++++++++++++++++++
        %Set scale bar and image rotation matrix info
        frameDiff=2;
        tempScale=frameDiff * 3 / 60; % the temporal scale for the speed proxy calculations in min (3 sec our frame interval)
        retractionThresh=-150;
        distanceScale = 0.4389 * 2 * (10/60) *1.5;  %=0.21 um/pixel - converting between pixels and microns
        
        folder=[masterRoot infoStruct(iCond).folders{iFolder}];
        
        %Read and process images
        filenames1=getFilenames(folder,channel1);
        filenames2=getFilenames(folder,channel2);
        %makes sure that there are file names for two channels
        if ~isempty(filenames1) && ~isempty(filenames2)
            %as an artifact of canceling, occasionally an uneven
            % number of pictures are collected the following if
            %loop will make the filenames1&2 the same length
            if ~(length(filenames1)== length(filenames2))
                if length(filenames1) > length(filenames2)
                    filenames1=filenames1(1:length(filenames2));
                else
                    filenames2=filenames2(1:length(filenames1));
                end
            end
        end
        
        if ~boolRegExp(infoStruct(iCond).condition,'nostim')
            
            clear numBefore numAfter maskFinal CFPFinal YFPFinal SumProjectedMask p channelMap maskFinal
            clear edgePtL edgePtR edgeMaskL200 edgeMaskR200 edgeMaskL400 edgeMaskR400 edgeMaskL800 edgeMaskR800
            clear edgeMaskOB200 edgeMaskOB400 edgeMaskOB800 bwmask skeleton sumlength s elongation areatotal
            
            fprintf('Analyzing opsin experiment: %s\n', infoStruct(iCond).folders{iFolder});
            
            numBefore=Cdc42Dat.FramesBefore;
            numAfter=Cdc42Dat.FramesAfter;
            maskFinal=Cdc42Dat.maskFinal(1:numBefore+numAfter-1);
            CFPFinal=Cdc42Dat.CFPFinal(1:numBefore+numAfter-1);
            YFPFinal=Cdc42Dat.YFPFinal(1:numBefore+numAfter-1);
            
            SumProjectedMask=0;
            
            Cdc42CompDat(iFolder).folderPathCdc42= infoStruct(iCond).folders{iFolder};
            Cdc42CompDat(iFolder).framesNoStimed=numBefore;
            Cdc42CompDat(iFolder).framesStimed=numAfter;
            Cdc42CompDat(iFolder).ExpType=Cdc42Dat.ExpType;
            Cdc42CompDat(iFolder).WinLose=Cdc42Dat.WinLose;
            Cdc42CompDat(iFolder).maskFinal=maskFinal;
            
            for i=1:length(filenames1)
                SumProjectedMask=SumProjectedMask+Cdc42Dat.maskFinal{i};
            end
            
            try
                % get the parameters through the function
                [p]=channelMapParameters(SumProjectedMask);
                %compute the functionalized form of the channel
                channelMap=channelBranchSkeletonFunction(p);
                
                % vizualize the fiting for the 2 branches and the original skeleton
                % bwmask = im2bw(SumProjectedMask,1);  %binarize the mask
                % skeleton= bwskel(bwmask);
                % imagesc(skeleton)
                % hold on
                % t=-1.5:0.01:1.5; vals=channelMap.branches(p,t); plot(vals(1,:),vals(2,:),'r')
                % get the interception pts between the cell mask and channel Map
                
                % compute the tangent and the normal at this point
                for j=1:length(filenames1)
                    
                    try
                        [edgePtL,edgePtR,edgeMaskL200,edgeMaskR200]=getBifurcationParametrizedEdgePts(maskFinal{j},channelMap,p,200); % define masks containing a set num of pxs that is closest to the tangent
                        %imagesc(maskFinal{j}+edgeMaskL200+edgeMaskR200*2); pause(0.1);
                        [edgePtL,edgePtR,edgeMaskL400,edgeMaskR400]=getBifurcationParametrizedEdgePts(maskFinal{j},channelMap,p,400);
                        [edgePtL,edgePtR,edgeMaskL800,edgeMaskR800]=getBifurcationParametrizedEdgePts(maskFinal{j},channelMap,p,800);
                        
                        Cdc42CompDat(iFolder).PtEdgeLc1{j}=edgePtL(1); % rounded up
                        Cdc42CompDat(iFolder).PtEdgeLc2{j}=edgePtL(2); % rounded up
                        Cdc42CompDat(iFolder).PtEdgeRc1{j}=edgePtR(1); % rounded up
                        Cdc42CompDat(iFolder).PtEdgeRc2{j}=edgePtR(2); % rounded up
                        
                        Cdc42CompDat(iFolder).edgeMaskL200{j}=edgeMaskL200; % can be used for extracting area speed
                        Cdc42CompDat(iFolder).edgeMaskR200{j}=edgeMaskR200;
                        
                        Cdc42CompDat(iFolder).edgeMaskL400{j}=edgeMaskL400; % can be used for extracting area speed
                        Cdc42CompDat(iFolder).edgeMaskR400{j}=edgeMaskR400;
                        
                        Cdc42CompDat(iFolder).edgeMaskL800{j}=edgeMaskL800; % can be used for extracting area speed
                        Cdc42CompDat(iFolder).edgeMaskR800{j}=edgeMaskR800;
                        
                        %ratio at left front 200 points 
                        Ratio_Front_L_200(j)=(nansum(YFPFinal{j}(logical(edgeMaskL200))))/(nansum(CFPFinal{j}(logical(edgeMaskL200))));
                        %ratio at left front 400 points
                        Ratio_Front_L_400(j)=(nansum(YFPFinal{j}(logical(edgeMaskL400))))/(nansum(CFPFinal{j}(logical(edgeMaskL400))));
                        %ratio at left front 800 points
                        Ratio_Front_L_800(j)=(nansum(YFPFinal{j}(logical(edgeMaskL800))))/(nansum(CFPFinal{j}(logical(edgeMaskL800))));
                        
                        %ratio at right front 200 points 
                        Ratio_Front_R_200(j)=(nansum(YFPFinal{j}(logical(edgeMaskR200))))/(nansum(CFPFinal{j}(logical(edgeMaskR200))));
                        %ratio at right front 400 points
                        Ratio_Front_R_400(j)=(nansum(YFPFinal{j}(logical(edgeMaskR400))))/(nansum(CFPFinal{j}(logical(edgeMaskR400))));
                        %ratio at right front 800 points
                        Ratio_Front_R_800(j)=(nansum(YFPFinal{j}(logical(edgeMaskR800))))/(nansum(CFPFinal{j}(logical(edgeMaskR800))));
                        
                        Cdc42CompDat(iFolder).Ratio_FrontL200{j}=Ratio_Front_L_200(j);
                        Cdc42CompDat(iFolder).Ratio_FrontL400{j}=Ratio_Front_L_400(j);
                        Cdc42CompDat(iFolder).Ratio_FrontL800{j}=Ratio_Front_L_800(j);
                        
                        Cdc42CompDat(iFolder).Ratio_FrontR200{j}=Ratio_Front_R_200(j);
                        Cdc42CompDat(iFolder).Ratio_FrontR400{j}=Ratio_Front_R_400(j);
                        Cdc42CompDat(iFolder).Ratio_FrontR800{j}=Ratio_Front_R_800(j);
                        
                        %ratio for the cell
                        Ratio_Cell(j)=(nansum(YFPFinal{j}(logical(maskFinal{j}))))/(nansum(CFPFinal{j}(logical(maskFinal{j}))));
                        Cdc42CompDat(iFolder).Ratio_Cell{j}=Ratio_Cell(j);
                        
                        %ratio at edge back 200
                        Ratio_EdgeBack_200(j)=(nansum(YFPFinal{j}(logical(edgeMaskOB200))))/(nansum(CFPFinal{j}(logical(edgeMaskOB200))));
                        %ratio at edge back 400
                        Ratio_EdgeBack_400(j)=(nansum(YFPFinal{j}(logical(edgeMaskOB400))))/(nansum(CFPFinal{j}(logical(edgeMaskOB400))));
                        %ratio at edge back 800
                        Ratio_EdgeBack_800(j)=(nansum(YFPFinal{j}(logical(edgeMaskOB800))))/(nansum(CFPFinal{j}(logical(edgeMaskOB800))));
                        
                        Cdc42CompDat(iFolder).Ratio_Back200{j}=Ratio_EdgeBack_200(j);
                        Cdc42CompDat(iFolder).Ratio_Back400{j}=Ratio_EdgeBack_400(j);
                        Cdc42CompDat(iFolder).Ratio_Back800{j}=Ratio_EdgeBack_800(j);
                        
                        %effort in extracting skeleton from mask and
                        %extension proxy
                        bwmask{j}=imdilate(maskFinal{j},strel('disk',5)); %dilating so as to avoid side branches later on
                        skeleton{j}= bwskel(bwmask{j});
                        sumlength(j)=(sum(sum(skeleton{j})))*distanceScale;
                        
                        %find initial elongation and total area
                        s = regionprops(maskFinal{j},'MajorAxisLength','Area');
                        elongation(j)=(s.MajorAxisLength)*distanceScale; % in um
                        areatotal(j)=(s.Area)*distanceScale*distanceScale; % in um^2
                        
                        Cdc42CompDat(iFolder).Ratio_SkeletonLength{j}=sumlength(j);
                        Cdc42CompDat(iFolder).Ratio_Elongation{j}=elongation(j);
                        Cdc42CompDat(iFolder).Ratio_AreaTotal{j}=areatotal(j);

                    end
                end
            end
        end %~if boolRegExp(infoStruct(iCond).condition,'nostim')
    end % iFolder
    % save([masterRoot 'Cdc42-Cont-Comp' filesep fileName],'Cdc42CompDat','-v7.3');
    fprintf('num folders remaining: %i\n', length(infoStruct(iCond).folders)-iFolder);
    save([masterRoot 'Cdc42DatContComp' filesep fileName],'Cdc42CompDat','-v7.3');
end %iCond